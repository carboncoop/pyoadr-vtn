from rest_framework import serializers

from vtn.models import Site


class SiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = (
            "customer",
            "site_name",
            "site_id",
            "site_location_code",
            "ip_address",
            "site_address1",
            "city",
            "state",
            "post_code",
            "contact_name",
            "phone_number",
            "online",
            "reporting_status",
        )
