class InvalidVENRequest(Exception):
    """
    An exception class just for verify_ven_request().  It indicates that the
    request from the VEN is invalid.
    """

    pass


class NotOptedOut(Exception):
    """An exception to throw to complain about a request to opt a site back which
    hasn't previously opted out"""

    pass
