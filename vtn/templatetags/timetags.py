from datetime import datetime

import pytz
from django import template

register = template.Library()


@register.filter(name="toUnix")
def toUnix(time):
    if time != "":
        return int(time.strftime("%s")) * 1000
    else:
        pass


@register.filter(name="toUnixSlice")
def toUnixSlice(time):
    if time != "":
        temp = datetime.fromtimestamp(time, tz=pytz.utc)
        return int(temp.strftime("%s")) * 1000
    else:
        pass


@register.filter(name="changeStatus")
def change_event_status(event_status):
    if event_status == "far":
        return "Scheduled"
    else:
        return event_status
