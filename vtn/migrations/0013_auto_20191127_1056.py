# Generated by Django 2.1.11 on 2019-11-27 10:56
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [("vtn", "0012_merge_20191029_1356")]

    operations = [
        migrations.RemoveField(model_name="customer", name="hub_contact_id"),
        migrations.AddField(
            model_name="customer",
            name="external_id",
            field=models.CharField(blank=True, db_index=True, max_length=100),
        ),
    ]
