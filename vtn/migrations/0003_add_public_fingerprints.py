from hashlib import sha1
from hashlib import sha256

from django.db import migrations


def add_public_key_fingerprints(apps, schema_editor):
    """
    We can't import the models directly as it may be a newer
    version than this migration expects. We use the historical versions.
    """
    SiteCertificate = apps.get_model("vtn", "SiteCertificate")
    for site_cert in SiteCertificate.objects.filter(certificate_sha1_fingerprint=""):
        cert = site_cert.cert

        sha1_fingerprint = sha1((cert.certificate).encode("utf-8")).hexdigest()
        sha256_fingerprint = sha256((cert.certificate).encode("utf-8")).hexdigest()

        site_cert.certificate_sha1_fingerprint = sha1_fingerprint
        site_cert.certificate_sha256_fingerprint = sha256_fingerprint
        site_cert.save()


class Migration(migrations.Migration):

    dependencies = [("vtn", "0002_add_public_key_fingerprint_fields")]

    operations = [migrations.RunPython(add_public_key_fingerprints)]
