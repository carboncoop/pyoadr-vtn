# Generated by Django 2.2.7 on 2020-02-04 17:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vtn', '0014_auto_20200123_1404'),
    ]

    operations = [
        migrations.AlterField(
            model_name='siteevent',
            name='event',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='vtn.Event'),
        ),
    ]
