# Generated by Django 2.2.7 on 2020-01-23 14:04
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("vtn", "0013_auto_20191127_1056"),
    ]

    operations = [
        migrations.AlterField(
            model_name="site",
            name="post_code",
            field=models.CharField(blank=True, max_length=10, verbose_name="Post Code"),
        ),
    ]
