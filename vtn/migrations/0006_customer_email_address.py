# Generated by Django 2.1.9 on 2019-09-09 15:34
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [("vtn", "0005_add_specifier_to_reports")]

    operations = [
        migrations.AddField(
            model_name="customer",
            name="email_address",
            field=models.EmailField(blank=True, max_length=254, null=True),
        )
    ]
