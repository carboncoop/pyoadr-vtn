from django.views.generic import TemplateView


class HealthCheck(TemplateView):
    template_name = "healthcheck.html"
