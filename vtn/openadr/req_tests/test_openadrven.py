import gevent
import pytest
import requests
from volttron.platform import get_services_core

AGENT_CONFIG = {"db_path": "$VOLTTRON_HOME/data/openadr.sqlite"}

web_address = ""


@pytest.fixture(scope="module")
def agent(request, volttron_instance_module_web):
    """Create a test agent that interacts with other agents (e.g., the OpenADRVenAgent)."""
    ven_agent = volttron_instance_module_web.build_agent()

    # Install and start an Open ADR VEN agent.
    agent_id = volttron_instance_module_web.install_agent(
        agent_dir="services/core/OpenADRVenAgent",
        config_file=AGENT_CONFIG,
        vip_identity="test_venagent",
        start=True,
    )
    print("OpenADR agent id: ", agent_id)

    def stop():
        volttron_instance_module_web.stop_agent(agent_id)
        ven_agent.core.stop()

    gevent.sleep(3)  # wait for agents and devices to start
    request.addfinalizer(stop)
    return ven_agent


class TestOpenADRVenAgent:
    """Regression tests for the Open ADR VEN Agent."""

    # @todo While testing, employ simulated devices and actuation/control agents

    def test_openadr(self, agent):
        # Test that a GET of the "oadr_request_event" XML succeeds, returning a response with a 200 status code
        url = "{}/OpenADR2/Simple/2.0b/EIEvent".format(web_address)
        xml_filename = get_services_core("OpenADRVenAgent/tests/oadr_request_event.xml")
        xml_file = open(xml_filename, "rb")
        headers = {"content-type": "application/sep+xml"}
        response = requests.get(url, data=xml_file, headers=headers)
        assert response.status_code == 200

        # Test that the PUT caused an update to the agent's list of events
        assert (
            agent.vip.rpc.call("test_ven_agent", "get_events").get(timeout=10) is None
        )
