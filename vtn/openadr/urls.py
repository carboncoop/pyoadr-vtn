from django.urls import path

from . import views


app_name = "vtn.openadr"

urlpatterns = [
    path("OpenADR2/Simple/2.0b/OadrPoll", views.OADRPoll.as_view(), name="oadr-poll"),
    path("OpenADR2/Simple/2.0b/EiEvent", views.EIEvent.as_view(), name="ei-event"),
    path("OpenADR2/Simple/2.0b/EiReport", views.EIReport.as_view(), name="ei-report"),
    # path(
    #     'OpenADR2/Simple/2.0b/EiRegisterParty',
    #     views.EIRegisterParty.as_view(),
    #     name='register-party'
    # ),
]
