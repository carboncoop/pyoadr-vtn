from django.apps import AppConfig


class OpenAdrConfig(AppConfig):
    name = "vtn.openadr"
