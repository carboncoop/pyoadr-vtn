from __future__ import unicode_literals

from datetime import datetime

from django.conf import settings
from django.utils import timezone
from isodate import isoduration

from vtn.openadr.static_methods import build_ei_response
from vtn.openadr.xsd import oadr_20b

SCHEMA_VERSION = "2.0b"
BOGUS_REQUEST_ID = 300
STATUS_OK = 200


class PayloadXML:
    def __init__(self, name):
        self.wrapped_object_name = name

    def wrap(self):
        kwargs = {self.wrapped_object_name: self.build()}
        signed_object = oadr_20b.oadrSignedObject(Id=None, **kwargs)
        payload = oadr_20b.oadrPayload(Signature=None, oadrSignedObject=signed_object)
        return payload

    def build(self):
        raise NotImplementedError


class OADRResponseBuilder(PayloadXML):
    def __init__(
        self,
        schema_version,
        status_number,
        request_id,
        response_description=None,
        ven_id=None,
    ):
        super(self.__class__, self).__init__("oadrResponse")
        self.schema_version = schema_version
        self.status_number = status_number
        self.request_id = request_id
        self.response_description = response_description
        self.ven_id = ven_id

    def build(self):
        return oadr_20b.oadrResponseType(
            schemaVersion=self.schema_version,
            eiResponse=oadr_20b.EiResponseType(
                responseCode=self.status_number,
                responseDescription=self.response_description,
                requestID=self.request_id,
            ),
            venID=self.ven_id,
        )


class OADRDistributeEventBuilder(PayloadXML):
    """
    This class is used to compose an OADR Distribute Event instance.
    """

    def __init__(self, ven_id, site_events):
        super(self.__class__, self).__init__("oadrDistributeEvent")
        self.ven_id = ven_id
        self.site_events = site_events

    def build(self):
        return oadr_20b.oadrDistributeEventType(
            schemaVersion=SCHEMA_VERSION,
            eiResponse=build_ei_response(
                status=STATUS_OK, response_description="OK", request_id=BOGUS_REQUEST_ID
            ),
            requestID=BOGUS_REQUEST_ID,
            vtnID=settings.VTN_ID,
            oadrEvent=self.build_oadr_events(),
        )

    def build_oadr_events(self):
        oadr_events = []

        # build list of oadr events
        for site_event in self.site_events:
            oadr_events.append(
                oadr_20b.oadrEventType(
                    eiEvent=self.build_ei_event(site_event),
                    oadrResponseRequired="always",
                )
            )

        return oadr_events

    def build_ei_event(self, site_event):
        return oadr_20b.eiEventType(
            eventDescriptor=self.build_event_descriptor(site_event),
            eiActivePeriod=self.build_active_period(site_event),
            eiEventSignals=self.build_ei_signals(site_event),
        )

    @staticmethod
    def build_event_descriptor(site_event):
        event_id = site_event.event_id
        modification_number = site_event.event.modification_number

        modification_reason = site_event.event.modification_reason
        priority = 1
        test_event = False
        vtn_comment = None

        created_date_time = datetime.isoformat(datetime.now())
        created_date_time = created_date_time[0:-7]

        event_status = "far"
        if site_event.status == "cancelled" or site_event.status == "CANCELED":
            event_status = "cancelled"
        else:
            event_status = site_event.event.status

        return oadr_20b.eventDescriptorType(
            eventID=event_id,
            modificationNumber=modification_number,
            modificationReason=modification_reason,
            priority=priority,
            createdDateTime=created_date_time,
            eventStatus=event_status,
            testEvent=test_event,
            vtnComment=vtn_comment,
        )

    def build_active_period(self, site_event):
        return oadr_20b.eiActivePeriodType(
            properties=self.build_active_period_properties(site_event), components=None
        )

    @staticmethod
    def build_active_period_properties(site_event):
        # Calculate duration
        event_start = site_event.event.start
        event_end = site_event.event.end
        event_duration = event_end - event_start
        seconds = event_duration.seconds
        # datetime.timedelta only has a seconds property, so pass in seconds to duration
        duration = isoduration.Duration(seconds=seconds)
        iso_duration = isoduration.duration_isoformat(
            duration
        )  # duration in iso format
        duration = oadr_20b.DurationPropType(duration=iso_duration)

        x_ei_notification = oadr_20b.DurationPropType(duration=iso_duration)
        dtstart = oadr_20b.dtstart(date_time=event_start)
        properties = oadr_20b.properties(
            dtstart=dtstart,
            duration=duration,
            x_eiNotification=x_ei_notification,
            x_eiRampUp=None,
            x_eiRecovery=None,
        )

        return properties

    def build_ei_signals(self, site_event):

        return oadr_20b.eiEventSignalsType(
            eiEventSignal=self.build_event_signal(site_event)
        )

    @staticmethod
    def build_event_signal(site_event):
        event_start = site_event.event.start
        event_end = site_event.event.end
        event_duration = event_end - event_start

        seconds = event_duration.seconds

        # To include the signal payload in the event signal
        # (i.e. event type: high, moderate etc.)
        # we must instanciate the following..
        signal_payload = site_event.event.signal_payload
        current_value = oadr_20b.currentValueType()
        current_value.payloadFloat = oadr_20b.PayloadFloatType()
        current_value.payloadFloat.set_value(signal_payload)

        # datetime.timedelta only has a seconds property, so pass in seconds to duration
        duration = isoduration.Duration(seconds=seconds)

        iso_duration = isoduration.duration_isoformat(
            duration
        )  # duration in iso format
        duration = oadr_20b.DurationPropType(duration=iso_duration)
        dtstart = oadr_20b.dtstart(date_time=event_start)
        properties = oadr_20b.properties(dtstart=dtstart, duration=duration)
        oadr_20b.WsCalendarIntervalType(properties=properties)
        interval = [oadr_20b.IntervalType(dtstart=dtstart, duration=duration)]
        intervals = oadr_20b.intervals(interval=interval)
        return [
            oadr_20b.eiEventSignalType(
                intervals=intervals,
                signalName="simple",
                signalType="level",
                currentValue=current_value,
            )
        ]


class OADRRegisteredReportBuilder(PayloadXML):
    """
    This class builds a RegisteredReport instance to convert to XML and pass back to the VEN.
    An instance of this class is created after the VTN receives an oadrRegisterReport.

    :param reports: A list of reports to request from the other party.
    """

    def __init__(self, ven_id, reports=None):
        super(self.__class__, self).__init__("oadrRegisteredReport")
        self.ven_id = ven_id
        self.reports = reports

    def build(self):
        return oadr_20b.oadrRegisteredReportType(
            schemaVersion=SCHEMA_VERSION,
            eiResponse=build_ei_response(
                status=STATUS_OK, response_description="OK", request_id=BOGUS_REQUEST_ID
            ),
            oadrReportRequest=[
                oadr_20b.oadrReportRequestType(
                    reportRequestID=report.report_request_id,
                    reportSpecifier=self.build_report_specifier(
                        report.report_specifier_id
                    ),
                )
                for report in self.reports
            ],
            venID=self.ven_id,
        )

    @staticmethod
    def build_report_specifier(report_specifier_id):
        # We send duration=None to indicate that the report should continue indefinitely
        # i.e. until superseded by another report request.  (OADR2.0b, p38)

        dtstart = oadr_20b.dtstart(date_time=timezone.now())
        properties = oadr_20b.properties(dtstart=dtstart, duration=None)
        report_interval = oadr_20b.WsCalendarIntervalType(properties=properties)

        # According to the schema, we should also send:
        # oadr:reportBackDuration
        # ei:specifierPayload
        return oadr_20b.ReportSpecifierType(
            reportSpecifierID=report_specifier_id, reportInterval=report_interval
        )
