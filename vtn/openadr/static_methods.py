from __future__ import unicode_literals

from io import StringIO

from vtn.openadr.xsd import oadr_20b


def get_payload_xml(oadr_payload):
    buff = StringIO()
    oadr_payload.export(buff, 1, pretty_print=True)
    return buff.getvalue()


def build_oadr_response(
    schema_version, status_number, request_id, response_description=None, ven_id=None
):

    return oadr_20b.oadrResponseType(
        schemaVersion=schema_version,
        eiResponse=oadr_20b.EiResponseType(
            responseCode=status_number,
            responseDescription=response_description,
            requestID=request_id,
        ),
        venID=ven_id,
    )


def build_ei_response(status, response_description, request_id):
    return oadr_20b.EiResponseType(
        responseCode=status,
        responseDescription=response_description,
        requestID=request_id,
    )
