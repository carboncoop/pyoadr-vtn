from __future__ import unicode_literals

import logging
import uuid as uuid_lib
from io import StringIO

import pytz
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import BaseParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_xml.renderers import XMLRenderer

from vtn.exceptions import InvalidVENRequest
from vtn.models import Report
from vtn.models import SiteEvent
from vtn.models import Telemetry
from vtn.openadr.builders import OADRDistributeEventBuilder
from vtn.openadr.builders import OADRRegisteredReportBuilder
from vtn.openadr.builders import OADRResponseBuilder
from vtn.openadr.builders import PayloadXML
from vtn.openadr.xsd import oadr_20b
from vtn.services import update_last_status_time
from vtn.services import update_notification_sent_time
from vtn.services import verify_ven_request

SCHEMA_VERSION = "2.0b"
BOGUS_REQUEST_ID = 300


logging.basicConfig(
    format="%(asctime)s %(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p",
    level=logging.DEBUG,
)
logger = logging.getLogger(__name__)


def BogusResponse(component: str, reason: str, ven_id: str = None) -> Response:
    """
    A helper function for error responses to reduce boilerplate.

    `component` is only used for the logging.
    `reason` is sent back to the client.
    """
    logger.warning(f"Bogus response from {component}: {reason}")
    response = OADRResponseBuilder(
        SCHEMA_VERSION, 400, BOGUS_REQUEST_ID, reason, ven_id
    )
    return Response(response)


class OADRRenderer(XMLRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        if not isinstance(data, PayloadXML):
            raise ValueError("Wrong data provided to renderer")

        payload = data.wrap()
        buffer = StringIO()
        payload.export(buffer, 1, pretty_print=True)
        return buffer.getvalue()


class OADRParser(BaseParser):

    media_type = "application/xml"

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Parses the incoming bytestream as XML and returns the resulting data.
        TODO: This function needs protection with diffusedxml
        """

        return oadr_20b.parse(stream)


class OADRPoll(APIView):
    """
    Called when the VEN issues an oadrPoll. Responds back, right now, with
    a Distribute Event if there are any un-acknowledged site events.
    """

    parser_classes = (OADRParser,)
    renderer_classes = (OADRRenderer,)

    @csrf_exempt
    def post(self, request, format=None):
        """
        At the moment our endpoint only implements the oadrDistributeEvent response
        (see p.24 OpenADR 2.0b for information on this process).  You can find
        information on other potential things we could send VENs in
        8.6 OpenADR Poll (p.47).
        """

        # The request should always include a VEN ID
        try:
            ven_id = request.data.oadrSignedObject.oadrPoll.venID
        except AttributeError:
            return BogusResponse("OADRPoll", "Request has no VEN ID")

        try:
            site = verify_ven_request(ven_id, request)
        except InvalidVENRequest as exc:
            return BogusResponse("OADRPoll", str(exc))

        # A site with that VEN ID exists

        pending_events = SiteEvent.objects.filter(
            ~Q(ven_status=SiteEvent.VEN_STATUS_ACK),
            site=site,
            event__scheduled_notification_time__lt=timezone.now(),
            event__end__gt=timezone.now(),
        ).count()

        # Do we have events to send?
        if pending_events > 0:
            build_events = SiteEvent.objects.filter(
                site=site,
                event__scheduled_notification_time__lt=timezone.now(),
                event__end__gt=timezone.now(),
            )

            # Update the notification_sent_time of the involved site events
            update_notification_sent_time(build_events)
            update_last_status_time(ven_id)
            logger.info("VTN sent distribute event")

            # Return OADR distribute event
            response = OADRDistributeEventBuilder(
                ven_id=ven_id, site_events=build_events
            )
            return Response(response)

        # Nothing to return at this point - return normal status with an empty oadr_response
        else:
            update_last_status_time(ven_id)

            response = OADRResponseBuilder(
                SCHEMA_VERSION, 200, BOGUS_REQUEST_ID, "No events to send", ven_id
            )
            return Response(response)


class EIReport(APIView):
    """
    VEN requests that are coming this way:
        - oadrRegisterReport
        - oadrCreatedReport (in response to oadrRegisterReport)
        - oadrUpdateReport
        - oadrCanceledReport
    """

    parser_classes = (OADRParser,)
    renderer_classes = (OADRRenderer,)

    def handle_register_report(self, request):
        """
        The oadrRegisterReport payload contains one or more metadata reports.
        These tell the VTN what reports the VEN is able to generate.

        This payload can be sent at any time, and it can (and will) be sent more than
        once.  So calling this function repeatedly with the same data should have
        no effect after the first time.

        You can read more about the process on the Carbon Co-op 'OpenADR notes' page:
        https://carboncoop.gitlab.io/opendsr/docs/system_components/openadr_notes.html

        Relevant conformance rules are OADR rule 301, OADR rule 309 and OADR rule 311.
        """

        try:
            ven_id = request.data.oadrSignedObject.oadrRegisterReport.venID
            oadr_reports = request.data.oadrSignedObject.oadrRegisterReport.oadrReport
        except AttributeError:
            return BogusResponse("RegisterReport", "Attributes missing")

        try:
            verify_ven_request(ven_id, request)
        except InvalidVENRequest as exc:
            return BogusResponse("RegisterReport", str(exc))

        update_last_status_time(ven_id)

        # The VEN can tell us it's capable of multiple reports.  We ask, greedily, for
        # all of them.
        #
        # But only if they're not already in our database.
        #
        # TODO: If we are sent oadrRegisterReport by a given VEN and it doesn't mention
        # some reports that are in our database, should we delete them?  Probably.
        try:
            reports = [
                Report.objects.create(
                    report_specifier_id=report.reportSpecifierID,
                    report_request_id=str(uuid_lib.uuid4()),
                    ven_id=ven_id,
                    report_status="active",
                )
                for report in oadr_reports
                if not Report.objects.filter(
                    ven_id=ven_id, report_specifier_id=report.reportSpecifierID
                ).exists()
            ]
        except AttributeError:
            return BogusResponse(
                "RegisterReport", "Report specifier ID not provided", ven_id
            )

        response = OADRRegisteredReportBuilder(ven_id, reports)
        return Response(response)

    def handle_created_report(self, request):

        try:
            oadr_created_report = request.data.oadrSignedObject.oadrCreatedReport
            ven_id = oadr_created_report.venID
            request_id = oadr_created_report.eiResponse.requestID
            report_request_ids = oadr_created_report.oadrPendingReports.reportRequestID
        except AttributeError:
            return BogusResponse("CreatedReport", "Attributes missing")

        try:
            verify_ven_request(ven_id, request)
        except InvalidVENRequest as exc:
            return BogusResponse("CreatedReport", str(exc))

        for report_id in report_request_ids:
            try:
                Report.objects.get(report_request_id=report_id)
            except ObjectDoesNotExist:
                logger.warning(f"Report with report request ID {report_id} not found")

        response = OADRResponseBuilder(SCHEMA_VERSION, 200, request_id)
        return Response(response)

    def handle_update_report(self, request):
        try:
            ven_id = request.data.oadrSignedObject.oadrUpdateReport.venID
            request_id = request.data.oadrSignedObject.oadrUpdateReport.requestID
            oadr_reports = request.data.oadrSignedObject.oadrUpdateReport.oadrReport
        except AttributeError:
            return BogusResponse("UpdateReport", "Attributes missing")

        try:
            site = verify_ven_request(ven_id, request)
        except InvalidVENRequest as exc:
            return BogusResponse("UpdateReport", str(exc))

        for oadr_report in oadr_reports:
            try:
                intervals = oadr_report.intervals.interval
            except AttributeError:
                return BogusResponse("UpdateReport", "No intervals given")
            for interval in intervals:
                start = interval.dtstart.get_date_time()
                if start is not None:
                    start.replace(tzinfo=pytz.utc)

                    report_payloads = interval.streamPayloadBase
                    payload_dict = {}
                    for report_payload in report_payloads:
                        payload_dict[
                            report_payload.rID
                        ] = report_payload.payloadBase.value

                    Telemetry.objects.create(
                        site=site,
                        created_on=start,
                        reported_on=timezone.now(),
                        values=payload_dict,
                        report_specifier_id=oadr_report.reportSpecifierID,
                    )

        response = OADRResponseBuilder(SCHEMA_VERSION, 200, request_id)
        return Response(response)

    def handle_canceled_report(self, request):
        try:
            oadr_canceled_report = request.data.oadrSignedObject.oadrCanceledReport
            request_id = oadr_canceled_report.eiResponse.requestID
            reports_to_keep = oadr_canceled_report.oadrPendingReports.reportRequestID
            ven_id = oadr_canceled_report.venID
        except AttributeError:
            return BogusResponse("CanceledReport", "Attributes missing")

        try:
            verify_ven_request(ven_id, request)
        except InvalidVENRequest as exc:
            return BogusResponse("CanceledReport", str(exc))

        # See 8.3.2.4 Cancel Reports of OpenADR 2.0b v1.1
        Report.objects.filter(
            Q(ven_id=ven_id),
            ~Q(report_status=Report.REPORT_CANCELLED),
            ~Q(report_request_id__in=reports_to_keep),
        ).update(report_status=Report.REPORT_CANCELLED)

        response = OADRResponseBuilder(SCHEMA_VERSION, 200, request_id)
        return Response(response)

    @csrf_exempt
    def post(self, request, format=None):

        if request.data.oadrSignedObject.oadrRegisterReport is not None:
            return self.handle_register_report(request)

        elif request.data.oadrSignedObject.oadrCreatedReport is not None:
            return self.handle_created_report(request)

        elif request.data.oadrSignedObject.oadrUpdateReport is not None:
            return self.handle_update_report(request)

        elif request.data.oadrSignedObject.oadrCanceledReport is not None:
            return self.handle_canceled_report(request)

        else:
            try:
                ven_response = request.data.oadrSignedObject.oadrResponse
                response_code = ven_response.eiResponse.responseCode
                request_id = ven_response.eiResponse.requestID
            except AttributeError:
                return BogusResponse("EIReport", "Could not parse VEN error code")

            logger.warning(f"VEN sent an error response code of {response_code}")

            response = OADRResponseBuilder(
                SCHEMA_VERSION, 204, request_id, "VEN sent an error code"
            )
            return Response(response)


class EIEvent(APIView):
    parser_classes = (OADRParser,)
    renderer_classes = (OADRRenderer,)

    def handle_request_event(self, request):
        try:
            ven_id = request.data.oadrSignedObject.oadrRequestEvent.eiRequestEvent.venID
        except Exception:
            return BogusResponse("RequestEvent", "Attributes missing")

        try:
            site = verify_ven_request(ven_id, request)
        except InvalidVENRequest as exc:
            return BogusResponse("RequestEvent", str(exc))

        events = SiteEvent.objects.filter(
            site=site,
            event__scheduled_notification_time__lt=timezone.now(),
            event__end__gt=timezone.now(),
        )

        update_notification_sent_time(events)
        update_last_status_time(ven_id)
        logger.info("VTN responded to RequestEvent")

        response = OADRDistributeEventBuilder(ven_id, events)
        return Response(response)

    def handle_created_event(self, request):
        try:
            oadr_created_event = request.data.oadrSignedObject.oadrCreatedEvent
            ven_id = oadr_created_event.eiCreatedEvent.venID
            request_id = oadr_created_event.eiCreatedEvent.eiResponse.requestID
            event_responses = (
                oadr_created_event.eiCreatedEvent.eventResponses.get_eventResponse()
            )
        except AttributeError:
            return BogusResponse("CreatedEvent", "Attributes missing")

        try:
            site = verify_ven_request(ven_id, request)
        except InvalidVENRequest as exc:
            return BogusResponse("CreatedEvent", str(exc))

        for event_response in event_responses:
            try:
                event_id = event_response.qualifiedEventID.eventID
                opt_response = event_response.optType
            except AttributeError:
                return BogusResponse("CreatedEvent", "No event ID or opt response")

            SiteEvent.objects.filter(event_id=event_id, site=site).update(
                ven_status=SiteEvent.VEN_STATUS_ACK,
                last_status_time=timezone.now(),
                opt_in=opt_response,
                last_opt_in=timezone.now(),
            )

            update_last_status_time(ven_id)

            response = OADRResponseBuilder(
                SCHEMA_VERSION,
                200,
                request_id,
                "Saved 'acknowledged' VEN status and updated statuses",
                ven_id,
            )
            return Response(response)

    @csrf_exempt
    def post(self, request, format=None):

        if request.data.oadrSignedObject.oadrRequestEvent is not None:
            return self.handle_request_event(request)

        elif request.data.oadrSignedObject.oadrCreatedEvent is not None:
            return self.handle_created_event(request)

        else:
            response_description = "VEN sent an error code"
            response_code = (
                request.data.oadrSignedObject.oadrResponse.eiResponse.responseCode
            )
            logger.warning(
                "VEN sent an error response code of {}".format(response_code)
            )

            response = OADRResponseBuilder(
                SCHEMA_VERSION, 204, BOGUS_REQUEST_ID, response_description
            )
            return Response(response)
