import os
import sys

import django
import pytest
import pytz
from django.conf import settings
from django.test import Client
from django.test import TestCase
from django_x509.models import Ca

from .helper_functions import create_event
from .helper_functions import get_file_xml
from .helper_functions import suppress_output
from .setup_test_data import create_customers
from .setup_test_data import create_programs
from .setup_test_data import create_sites
from vtn.models import Event
from vtn.models import Report
from vtn.models import Site
from vtn.models import SiteEvent
from vtn.models import Telemetry
from vtn.openadr.xsd import oadr_20b
from vtn.services import generate_cert


pytestmark = pytest.mark.django_db

django.setup()

TEST_DIR = os.path.dirname(os.path.abspath(__file__))
XML_DIR = os.path.join(TEST_DIR, "xml/")
BASE_URL = "http://127.0.0.1:8000"
POLL_URL = "/OpenADR2/Simple/2.0b/OadrPoll"
EVENT_URL = "/OpenADR2/Simple/2.0b/EiEvent"
REPORT_URL = "/OpenADR2/Simple/2.0b/EiReport"


class TestFingerprintVerification(TestCase):
    @classmethod
    def setUpTestData(cls):
        create_customers(5)
        create_programs(2)
        create_sites(2)
        site = Site.objects.first()
        create_event(site.ven_id, "active", "not_told", "1")
        ca = Ca.objects.create(name=settings.CA_NAME)
        cls.site_cert = generate_cert(ca, site)

    def test_one_event_correct(self):
        response = Client().post(
            POLL_URL,
            get_file_xml("ven_poll"),
            content_type="application/xml",
            HTTP_X_CERTIFICATE_FINGERPRINT_SHA1=self.site_cert.certificate_sha1_fingerprint,
        )
        parsed = oadr_20b.parseString(response.content)

        assert parsed.oadrSignedObject.oadrDistributeEvent is not None

    def test_one_event_incorrect(self):
        response = Client().post(
            POLL_URL,
            get_file_xml("ven_poll"),
            content_type="application/xml",
            HTTP_X_CERTIFICATE_FINGERPRINT_SHA1=self.site_cert.certificate_sha1_fingerprint
            + "1",
        )
        parsed = oadr_20b.parseString(response.content)

        assert parsed.oadrSignedObject.oadrResponse is not None
        assert parsed.oadrSignedObject.oadrResponse.eiResponse.responseCode == str(400)
        assert (
            "not recognised"
            in parsed.oadrSignedObject.oadrResponse.eiResponse.responseDescription
        )


class TestDialogue(TestCase):
    """
    This class tests the various features of the VTN.
    """

    @classmethod
    def setUpTestData(cls):
        create_customers(5)
        create_programs(2)
        create_sites(2)

    # Test reporting #

    def test_register_report(self):
        """
        This checks to make sure a report is correctly recorded in the database
        after a RegisterReport is sent to the VTN.
        """
        register_report_xml = get_file_xml("ven_register_report")
        response = Client().post(
            REPORT_URL, register_report_xml, content_type="application/xml"
        )
        parsed = oadr_20b.parseString(response.content)

        report = Report.objects.get()
        assert report.ven_id == "0"
        assert report.report_status == "active"
        assert report.report_specifier_id == "telemetry"
        assert parsed.oadrSignedObject.oadrRegisteredReport is not None

    def test_add_register_report(self):
        """
        When a report is registered that has been registered before (which we can tell
        because the combination of (ven_id, report_specifier_id) are unique), we
        shouldn't re-register it.
        """
        Report.objects.create(
            ven_id="0",
            report_specifier_id="telemetry",
            report_request_id="FEREDFSGWRGRGSD",
            report_status="active",
        )

        register_report_xml = get_file_xml("ven_register_report")
        Client().post(REPORT_URL, register_report_xml, content_type="application/xml")

        assert Report.objects.count() == 1

    def test_created_and_canceled_report(self):
        """
        This checks to make sure the VTN is correctly processing CreatedReport
        and CanceledReport.
        """
        Report.objects.all().delete()  # Clean the slate
        Report(ven_id="0", report_request_id="0", report_status="active").save()
        created_report_xml = get_file_xml("ven_created_report")
        vtn_response = get_file_xml("vtn_200_response")
        request_id = "c206f5a8-e1c3-11e7-91ae-6c96cfdb28b5"  # Can change this as long as corresponding xml file
        # is changed as well
        client = Client()
        created_report_response = client.post(
            REPORT_URL, created_report_xml, content_type="application/xml"
        )
        vtn_response = vtn_response.format(request_id=request_id)
        canceled_report_xml = get_file_xml("ven_canceled_report")
        client.post(REPORT_URL, canceled_report_xml, content_type="application/xml")
        report = Report.objects.get(report_request_id="0")
        self.assertEqual(report.report_status, "cancelled")
        self.assertXMLEqual(
            vtn_response, created_report_response.content.decode("utf-8")
        )

    @pytest.mark.skip(reason="Telemetry object format has changed, and is still a WIP")
    def test_update_report(self):
        """
        This checks if the correct telemetry in UpdateReport has been recorded in the database.
        """
        Telemetry.objects.all().delete()  # Delete any existing Telemetry objects
        update_report_xml = get_file_xml("ven_update_report")
        old_stdout = suppress_output()
        parsed_update_report = oadr_20b.parseString(bytes(update_report_xml, "utf-8"))
        sys.stdout = old_stdout
        oadr_reports = parsed_update_report.oadrSignedObject.oadrUpdateReport.oadrReport
        client = Client()
        client.post(REPORT_URL, update_report_xml, content_type="application/xml")
        t_data = Telemetry.objects.all()
        t_data = t_data[0]
        baseline_power = 0
        actual_power = 0
        for oadr_report in oadr_reports:
            intervals = oadr_report.intervals.interval
            for interval in intervals:
                start = interval.dtstart.get_date_time()
                start.replace(tzinfo=pytz.utc)
                if start is not None:
                    report_payloads = interval.streamPayloadBase
                    for report_payload in report_payloads:
                        rID = report_payload.rID
                        if rID == "baseline_power":
                            baseline_power = report_payload.payloadBase.value
                        elif rID == "actual_power":
                            actual_power = report_payload.payloadBase.value
        self.assertEqual(baseline_power, t_data.baseline_power_kw)
        self.assertEqual(actual_power, t_data.measured_power_kw)
        self.assertEqual("0", t_data.site.ven_id)

    # Test Events #

    def test_no_events(self):
        """
        This checks that when a VEN poll is sent to the VTN, the VTN responds
        with an empty response when there are no applicable DR Events.
        """
        Event.objects.all().delete()  # Clean the slate
        vtn_response_xml = get_file_xml("vtn_response_no_events")
        poll_xml = get_file_xml("ven_poll")
        client = Client()
        response = client.post(POLL_URL, poll_xml, content_type="application/xml")
        self.assertXMLEqual(vtn_response_xml, response.content.decode("utf-8"))

        create_event("0", "active", "not_told", "1")
        poll_xml = poll_xml.replace("<ei:venID>0</ei:venID>", "<ei:venID>1</ei:venID>")
        response = client.post(POLL_URL, poll_xml, content_type="application/xml")
        vtn_response_xml = vtn_response_xml.replace(
            "<oadr:venID>0</oadr:venID>", "<oadr:venID>1</oadr:venID>"
        )
        self.assertXMLEqual(vtn_response_xml, response.content.decode("utf-8"))

    def test_one_event(self):
        """
        This checks that a distribute event is correctly returned by the VTN when there is an
        un-acknowledged site-event for the site with ven_id '0'.
        """

        Event.objects.all().delete()  # clean the slate
        # Create a DR Event for Site with ven_id '0'
        create_event("0", "active", "not_told", "1")

        poll_xml = get_file_xml("ven_poll")
        client = Client()
        response = client.post(POLL_URL, poll_xml, content_type="application/xml")
        old_stdout = suppress_output()
        parsed = oadr_20b.parseString(response.content)
        sys.stdout = old_stdout

        site_event = SiteEvent.objects.get(site__ven_id="0")

        self.assertEqual(site_event.ven_status, "told")
        self.assertIsNotNone(parsed.oadrSignedObject.oadrDistributeEvent)

    def test_two_events(self):
        """
        This checks that a distribute event is correctly returned by the VTN when there
        is more than one un-acknowledged site-event for the site with ven_id '0'.
        """
        Event.objects.all().delete()  # clean the slate
        create_event("0", "active", "not_told", "1")
        create_event("0", "active", "told", "1")

        poll_xml = get_file_xml("ven_poll")
        client = Client()
        response = client.post(POLL_URL, poll_xml, content_type="application/xml")
        old_stdout = suppress_output()
        parsed = oadr_20b.parseString(response.content)
        sys.stdout = old_stdout

        oadr_events = parsed.oadrSignedObject.oadrDistributeEvent.oadrEvent

        self.assertEqual(len(oadr_events), 2)

    def test_ack_events(self):
        """
        This checks that events that the VEN has already acknowledged aren't re-sent
        by the VTN.
        """

        Event.objects.all().delete()
        create_event("0", "active", "acknowledged", "1")
        create_event("0", "active", "acknowledged", "1")

        poll_xml = get_file_xml("ven_poll")
        client = Client()
        response = client.post(POLL_URL, poll_xml, content_type="application/xml")
        vtn_response_xml = get_file_xml("vtn_response_no_events")
        self.assertXMLEqual(vtn_response_xml, response.content.decode("utf-8"))

    def test_inconsistent_ack_events(self):
        """
        This checks that the VTN sends all applicable events to the VEN, even if some
        events have been acknowledged by the VEN and some have not.
        """

        Event.objects.all().delete()
        create_event("0", "active", "acknowledged", "1")
        create_event("0", "active", "not_told", "1")

        poll_xml = get_file_xml("ven_poll")
        client = Client()
        response = client.post(POLL_URL, poll_xml, content_type="application/xml")
        old_stdout = suppress_output()
        parsed = oadr_20b.parseString(response.content)
        sys.stdout = old_stdout

        oadr_events = parsed.oadrSignedObject.oadrDistributeEvent.oadrEvent

        site_events = SiteEvent.objects.all()
        for site_event in site_events:
            self.assertEqual(site_event.ven_status, "told")

        self.assertEqual(len(oadr_events), 2)

    def test_canceled_event(self):
        """
        This checks that a canceled event with the 'cancelled' status is sent to VEN upon
        a poll.
        """

        Event.objects.all().delete()  # Clear the slate
        create_event("0", "cancelled", "not_told", "1")

        poll_xml = get_file_xml("ven_poll")
        client = Client()
        response = client.post(POLL_URL, poll_xml, content_type="application/xml")
        old_stdout = suppress_output()
        parsed = oadr_20b.parseString(response.content)
        sys.stdout = old_stdout
        event_status = ""

        oadr_events = parsed.oadrSignedObject.oadrDistributeEvent.oadrEvent
        for oadr_event in oadr_events:  # There is guaranteed to be only one oadr_event
            event_status = oadr_event.eiEvent.eventDescriptor.eventStatus
        self.assertEqual(event_status, "cancelled")

    def test_created_event(self):
        """
        Tests that the VTN updates a site event's status upon receipt of
        a CreatedEvent.
        """

        Event.objects.all().delete()  # clean the slate
        event_id = create_event("0", "active", "not_told", "1")
        created_event_xml = get_file_xml("ven_created_event")
        created_event_xml = created_event_xml.replace("!!!EVENT_ID!!!", str(event_id))
        client = Client()
        response = client.post(
            EVENT_URL, created_event_xml, content_type="application/xml"
        )
        old_stdout = suppress_output()
        oadr_20b.parseString(response.content)
        sys.stdout = old_stdout

        site_event = SiteEvent.objects.get(site__ven_id="0")

        self.assertEqual(site_event.opt_in, "optIn")
        self.assertEqual(site_event.ven_status, "acknowledged")

    def test_invalid_ven_id(self):
        """
        Tests that the VTN returns proper response when the VEN's ven_id doesn't
        exist in the VTN's database.
        """
        Event.objects.all().delete()  # clean the slate
        poll_xml = get_file_xml("ven_poll_ven_id_too_high")
        vtn_response_xml = get_file_xml("vtn_no_site_found")
        client = Client()
        response = client.post(POLL_URL, poll_xml, content_type="application/xml")
        self.assertEqual(response.status_code, 200)
        self.assertXMLEqual(vtn_response_xml, response.content.decode("utf-8"))

    def test_signal_payload_sent(self):
        """
        Tests that the signal payload (i.e. type of event) is sent in the Event
        """

        Event.objects.all().delete()  # clean the slate
        # Create a DR Event for Site with ven_id '0'
        create_event("0", "active", "not_told", "1")

        poll_xml = get_file_xml("ven_poll")
        client = Client()
        response = client.post(POLL_URL, poll_xml, content_type="application/xml")
        parsed = oadr_20b.parseString(response.content)

        oadr_events = parsed.oadrSignedObject.oadrDistributeEvent.oadrEvent
        for oadr_event in oadr_events:
            signals = oadr_event.eiEvent.eiEventSignals.eiEventSignal
            for signal in signals:
                payload_float = signal.currentValue.payloadFloat.value

        self.assertEqual(payload_float, float("1.0"))
