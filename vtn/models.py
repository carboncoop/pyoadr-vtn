from __future__ import unicode_literals

import uuid as uuid_lib

from django.contrib.postgres.fields import JSONField
from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator
from django.db import models
from django.urls import reverse_lazy
from django_x509.models import Cert

MAX_LENGTH_TEXT = 100
MAX_LENGTH_PHONE = 20
MAX_LENGTH_POSTCODE = 10


class Customer(models.Model):
    """
    An OpenADR Customer.
    A customer can own many sites.

    :param name: Name of the customer
    :param external_id: An external reference ID used when integrating with other systems
    :param utility_id: Unknown. Not currently used by Carbon Coop
    :param contact_name: Contact name
    :param phone_number: Contact phone number
    :param email_address: Contact email address
    """

    name = models.CharField("Name", db_index=True, max_length=MAX_LENGTH_TEXT)
    external_id = models.CharField(
        db_index=True, max_length=MAX_LENGTH_TEXT, blank=True
    )
    utility_id = models.CharField("Utility ID", max_length=MAX_LENGTH_TEXT, blank=True)
    contact_name = models.CharField(
        "Contact Name", max_length=MAX_LENGTH_TEXT, blank=True
    )
    phone_number = models.CharField(
        "Phone Number", max_length=MAX_LENGTH_PHONE, blank=True
    )
    email_address = models.EmailField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse_lazy("vtn:customer_detail", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

    def sites(self):
        return self.site_set.all()


class Program(models.Model):
    class Meta:
        verbose_name_plural = "Programs"
        verbose_name = "Program"

    name = models.CharField("Program Name", max_length=MAX_LENGTH_TEXT, unique=True)
    sites = models.ManyToManyField("Site", blank=True)
    notes = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Site(models.Model):
    """An OpenADR site.

    :param site_id: A unique identifier. Not currently the primary key, but maybe it could be
    :param customer: The customer the site belongs to
    :param ven_id: The Virtual End Node (VEN) unique ID. There is currently no seperate VEN model.
    :param site_name: A name for the site
    :param external_id: Not currently used at Carbon Coop
    :param site_location_code: Not currently used at Carbon Coop
    :param ven_name: Used if a ven registers itself - not used in Carbon Coop implementation
    :param ip_address: Not currently used at Carbon Coop
    :param site_address1: Address
    :param site_address2: Address
    :param city: City
    :param state: State
    :param post_code: Post code of the site location
    :param contact_name: Contact name - could be different than the customer who owns it
    :param phone_number: Phone number for the `contact_name`
    :param online: If the VEN has recently polled, it is online
    :param last_status_time: The time the VEN last contacted the VTN
    """

    site_id = models.UUIDField(db_index=True, default=uuid_lib.uuid4, editable=False)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=False)
    ven_id = models.CharField("VEN ID", max_length=MAX_LENGTH_TEXT, unique=True)

    site_name = models.CharField("Site Name", max_length=MAX_LENGTH_TEXT, blank=True)
    external_id = models.CharField(
        "External ID", max_length=MAX_LENGTH_TEXT, blank=True
    )
    site_location_code = models.CharField(
        "Site Location Code", max_length=MAX_LENGTH_TEXT, blank=True
    )
    ven_name = models.CharField("VEN Name", max_length=MAX_LENGTH_TEXT, blank=True)

    ip_address = models.CharField("IP address", max_length=MAX_LENGTH_TEXT, blank=True)

    site_address1 = models.CharField(
        "Address Line 1", max_length=MAX_LENGTH_TEXT, blank=True
    )
    site_address2 = models.CharField(
        "Address Line 2", max_length=MAX_LENGTH_TEXT, blank=True
    )
    city = models.CharField(verbose_name="City", max_length=MAX_LENGTH_TEXT, blank=True)
    state = models.CharField(
        verbose_name="Region/Province", max_length=MAX_LENGTH_TEXT, blank=True
    )
    post_code = models.CharField(
        "Post Code", max_length=MAX_LENGTH_POSTCODE, blank=True
    )

    contact_name = models.CharField(
        "Contact Name", max_length=MAX_LENGTH_TEXT, blank=True
    )
    phone_number = models.CharField(
        "Phone Number", max_length=MAX_LENGTH_PHONE, blank=True
    )

    online = models.BooleanField(default=False)
    last_status_time = models.DateTimeField("Last Status Time", blank=True, null=True)

    def get_absolute_url(self):
        return reverse_lazy("vtn:customer_detail", kwargs={"pk": self.customer.pk})

    def get_name_display(self):
        if self.site_name == "":
            if self.customer:
                return f"{self.customer.name}'s site"
            else:
                return "unnamed site"
        else:
            return self.site_name

    def __str__(self):
        return "({}) {}".format(self.customer.name, self.site_name)


class AbstractEvent(models.Model):
    class Meta:
        verbose_name_plural = "Events"
        verbose_name = "Event"
        abstract = True

    EVENT_SCHEDULED = "scheduled"
    EVENT_FAR = "far"
    EVENT_NEAR = "near"
    EVENT_ACTIVE = "active"
    EVENT_COMPLETED = "completed"
    EVENT_CANCELLED = "cancelled"
    EVENT_UNRESPONDED = "unresponded"

    STATUS_CHOICES = (
        (EVENT_SCHEDULED, "scheduled"),
        (EVENT_FAR, "far"),
        (EVENT_NEAR, "near"),
        (EVENT_ACTIVE, "active"),
        (EVENT_COMPLETED, "completed"),
        (EVENT_CANCELLED, "cancelled"),
        (EVENT_UNRESPONDED, "unresponded"),
    )

    program = models.ForeignKey(Program, on_delete=models.SET_NULL, null=True)
    scheduled_notification_time = models.DateTimeField("Scheduled Notification Time")
    start = models.DateTimeField("Event Start")
    end = models.DateTimeField("Event End")
    status = models.CharField(
        "Event Status",
        max_length=MAX_LENGTH_TEXT,
        choices=STATUS_CHOICES,
        default="far",
    )
    created_on = models.DateTimeField("Created", auto_now_add=True)
    modification_reason = models.CharField(max_length=MAX_LENGTH_TEXT, blank=True)

    def get_absolute_url(self):
        return reverse_lazy("vtn:home")


class Event(AbstractEvent):
    # TODO: Upgrade Django>3 in prod so we can use IntegerChoices

    # class SignalPayload(models.IntegerChoices):
    #     NORMAL = 0
    #     MODERATE = 1
    #     HIGH = 2
    #     SPECIAL = 3

    sites = models.ManyToManyField(Site, through="SiteEvent", related_name="Sites1")
    deleted_at = models.DateTimeField("Time deleted", blank=True, null=True)
    signal_payload = models.IntegerField(
        null=True, validators=[MinValueValidator(0), MaxValueValidator(4)]
    )

    def __str__(self):
        try:
            program_name = self.program.name
        except AttributeError:
            program_name = "Deleted program"
        return f"{program_name}: Event starts at {self.start}. Event ends at {self.end}"

    @property
    def modification_number(self):
        """Calculated field returning modification number per the OADR spec

        As this is zero-based counting (OADR rule 4) it simply returns the
        number of SupersededEvents
        """
        return self.supersededevent_set.count()


class SupersededEvent(AbstractEvent):
    class Meta:
        verbose_name_plural = "Superseded events"
        verbose_name = "Superseded event"

    event = models.ForeignKey(Event, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        try:
            program_name = self.program.name
        except AttributeError:
            program_name = "Deleted program"
        return (
            f"{program_name}: Superseded version of event {self.event_id} "
            f"starts at {self.start}. Event ends at {self.end}"
        )


class SiteEvent(models.Model):
    class Meta:
        verbose_name_plural = "Site Events"

    EVENT_SCHEDULED = "scheduled"
    EVENT_FAR = "far"
    EVENT_NEAR = "near"
    EVENT_ACTIVE = "active"
    EVENT_COMPLETED = "completed"
    EVENT_CANCELLED = "cancelled"
    EVENT_UNRESPONDED = "unresponded"

    STATUS_CHOICES = (
        (EVENT_SCHEDULED, "scheduled"),
        (EVENT_FAR, "far"),
        (EVENT_NEAR, "near"),
        (EVENT_ACTIVE, "active"),
        (EVENT_COMPLETED, "completed"),
        (EVENT_CANCELLED, "cancelled"),
        (EVENT_UNRESPONDED, "unresponded"),
    )

    OPT_IN = "optIn"
    OPT_OUT = "optOut"
    OPT_NEITHER = "none"

    OPT_IN_CHOICES = ((OPT_IN, "optIn"), (OPT_OUT, "optOut"), (OPT_NEITHER, "Neither"))

    VEN_STATUS_NOT_TOLD = "not_told"
    VEN_STATUS_TOLD = "told"
    VEN_STATUS_ACK = "acknowledged"

    VEN_STATUS_CHOICES = (
        (VEN_STATUS_NOT_TOLD, "not_told"),
        (VEN_STATUS_TOLD, "told"),
        (VEN_STATUS_ACK, "acknowledged"),
    )

    site = models.ForeignKey(Site, models.SET_NULL, null=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True)

    status = models.CharField(
        max_length=MAX_LENGTH_TEXT, choices=STATUS_CHOICES, default="scheduled"
    )
    notification_sent_time = models.DateTimeField(
        "Notification Sent Time", blank=True, null=True
    )
    last_status_time = models.DateTimeField("Last Status Time")
    modification_number = models.IntegerField("Modification Number", default=0)
    opt_in = models.CharField(
        max_length=MAX_LENGTH_TEXT, choices=OPT_IN_CHOICES, default="none"
    )
    ven_status = models.CharField(
        max_length=MAX_LENGTH_TEXT, choices=VEN_STATUS_CHOICES, default="not_told"
    )
    last_opt_in = models.DateTimeField("Last opt-in", blank=True, null=True)
    previous_version = models.ForeignKey("self", models.SET_NULL, blank=True, null=True)
    deleted = models.BooleanField(default=False)


class Telemetry(models.Model):
    class Meta:
        verbose_name_plural = "Telemetry"

    site = models.ForeignKey(Site, on_delete=models.SET_NULL, null=True)
    created_on = models.DateTimeField(auto_created=True)
    reported_on = models.DateTimeField(null=True, blank=True)
    values = JSONField(null=True, blank=True)
    baseline_power_kw = models.FloatField("Baseline Power (kw)", blank=True, null=True)
    measured_power_kw = models.FloatField("Measured Power (kw)", blank=True, null=True)
    report_specifier_id = models.CharField(
        verbose_name="Report Specifier ID",
        help_text="This is the ID that the VEN uses to identify this report.",
        max_length=MAX_LENGTH_TEXT,
        blank=True,
    )


class Report(models.Model):
    REPORT_ACTIVE = "active"
    REPORT_CANCELLED = "cancelled"
    REPORT_CANCELLED_REQUESTED = "cancelled_requested"

    REPORT_STATUS_CHOICES = (
        (REPORT_ACTIVE, "active"),
        (REPORT_CANCELLED, "cancelled"),
        (REPORT_CANCELLED_REQUESTED, "cancelled_requested"),
    )

    # A report is associated with a particular VEN
    ven_id = models.CharField("VEN ID", max_length=MAX_LENGTH_TEXT, blank=True)

    report_status = models.CharField(
        "Report Status",
        max_length=MAX_LENGTH_TEXT,
        choices=REPORT_STATUS_CHOICES,
        default="active",
    )

    report_specifier_id = models.CharField(
        verbose_name="Report Specifier ID",
        help_text="This is the ID that the VEN uses to identify this report.",
        max_length=MAX_LENGTH_TEXT,
    )

    report_request_id = models.CharField(
        verbose_name="Report Request ID",
        help_text="This is the ID that the VTN uses to uniquely identify this report.",
        max_length=MAX_LENGTH_TEXT,
        unique=True,
    )


class SiteCertificate(models.Model):
    site = models.ForeignKey(Site, on_delete=models.CASCADE, null=False)
    cert = models.ForeignKey(Cert, on_delete=models.CASCADE, null=False)
    certificate_sha1_fingerprint = models.TextField(null=False)
    certificate_sha256_fingerprint = models.TextField(null=False)
