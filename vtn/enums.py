import enum


class ExtendedEnum(enum.Enum):
    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))


class EventStatus(ExtendedEnum):
    UNRESPONDED = "unresponded"
    FAR = "far"
    NEAR = "near"
    ACTIVE = "active"
    CANCELLED = "cancelled"
    COMPLETED = "completed"
