import django
import factory
from django.contrib.auth.models import User
from faker import Faker


django.setup()

fake = Faker()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ("username",)

    # Defaults (can be overriden)
    username = "test"
    password = "password"


class AdminFactory(UserFactory):
    username = "admin"
    is_superuser = True
    is_staff = True
