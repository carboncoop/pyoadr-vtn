from datetime import timedelta

import pytest
from django.conf import settings
from django.http import HttpRequest
from django.test import TestCase
from django.utils import timezone
from django_x509.models import Ca

from vtn import models
from vtn import services
from vtn.exceptions import InvalidVENRequest
from vtn.openadr.tests import factories


class TestVerifyVENRequest(TestCase):
    def test_exception_if_ven_not_recognised(self):
        #  Don't create a VEN - this means "fred" is unrecognised
        request = HttpRequest()

        with pytest.raises(InvalidVENRequest):
            services.verify_ven_request("fred", request)

    def test_fingerprint_is_checked(self):
        customer = factories.CustomerFactory()
        site = factories.SiteFactory(customer=customer)
        ca = Ca.objects.create(name=settings.CA_NAME)
        services.generate_cert(ca, site)
        request = HttpRequest()
        request.META["HTTP_X_CERTIFICATE_FINGERPRINT_SHA1"] = "Fred"

        with pytest.raises(InvalidVENRequest):
            services.verify_ven_request(site.ven_id, request)

    def test_it_works(self):
        customer = factories.CustomerFactory()
        site = factories.SiteFactory(customer=customer)
        ca = Ca.objects.create(name=settings.CA_NAME)
        site_cert = services.generate_cert(ca, site)
        request = HttpRequest()
        request.META[
            "HTTP_X_CERTIFICATE_FINGERPRINT_SHA1"
        ] = site_cert.certificate_sha1_fingerprint

        services.verify_ven_request(site.ven_id, request)


class TestEventServices(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.customer = factories.CustomerFactory()
        cls.site1 = factories.SiteFactory(customer=cls.customer)
        cls.site2 = factories.SiteFactory(customer=cls.customer)
        cls.program = factories.ProgramFactory()
        cls.kickoff_time = timezone.now()
        cls.event = factories.EventFactory()
        cls.siteevent1 = models.SiteEvent.objects.create(
            site=cls.site1,
            event=cls.event,
            status=models.SiteEvent.EVENT_FAR,
            last_status_time=timezone.now(),
        )
        cls.siteevent2 = models.SiteEvent.objects.create(
            site=cls.site2,
            event=cls.event,
            status=models.SiteEvent.EVENT_CANCELLED,
            last_status_time=timezone.now(),
        )

    def test_event_and_siteevent_are_created(self):
        event_id = services.create_event(
            {
                "program": self.program,
                "scheduled_notification_time": self.kickoff_time,
                "start": self.kickoff_time + timedelta(minutes=5),
                "end": self.kickoff_time + timedelta(minutes=10),
                "status": "far",
            },
            [self.site1.id],
        )

        assert type(event_id) is int
        assert event_id > 0

        events = models.Event.objects.filter(pk=event_id)
        assert events.count() == 1
        assert events[0].id == event_id

        siteevents = models.SiteEvent.objects.filter(event_id=event_id)
        assert len(siteevents) == 1
        assert siteevents[0].site_id == self.site1.id

    def test_site_opts_out(self):
        modification_number_before = self.event.modification_number
        services.remove_site_from_event(
            event_id=self.event.id, remove_site_pk=self.site1.id
        )
        self.siteevent1.refresh_from_db()
        self.event.refresh_from_db()

        assert self.siteevent1.status == models.SiteEvent.EVENT_CANCELLED
        assert self.event.modification_number == modification_number_before + 1

    def test_site_resubscribes(self):
        modification_number_before = self.event.modification_number
        services.resubscribe_site_to_event(
            event_id=self.event.id, restore_site_id=self.site2.id
        )
        self.siteevent2.refresh_from_db()
        self.event.refresh_from_db()

        assert self.siteevent2.status == models.SiteEvent.EVENT_FAR
        assert self.event.modification_number == modification_number_before + 1

    def test_event_update_creates_superseded_event(self):
        modification_number_before = self.event.modification_number
        sites_count_before = self.event.sites.count()

        previous_start_time = self.event.start
        new_start_time = previous_start_time + timedelta(minutes=20)
        services.update_event(event_id=self.event.id, data={"start": new_start_time})

        self.event.refresh_from_db()

        assert self.event.modification_number == modification_number_before + 1
        assert self.event.start == new_start_time
        assert (
            models.SupersededEvent.objects.filter(event=self.event).count()
            == modification_number_before + 1
        )

        # Check it doesn't affect the number of sites. It really shouldn't.
        assert self.event.sites.count() == sites_count_before

    def test_event_status_updates_without_revision(self):
        # Changing the status to "far" shouldn't trigger a SupersededEvent

        modification_number_before = self.event.modification_number

        services.update_event(
            event_id=self.event.id, data={"status": models.Event.EVENT_FAR}
        )

        self.event.refresh_from_db()

        assert self.event.modification_number == modification_number_before

    def test_event_status_updates_with_revision(self):
        # Changing the status to "cancelled" should trigger a SupersededEvent

        modification_number_before = self.event.modification_number

        services.update_event(
            event_id=self.event.id, data={"status": models.Event.EVENT_CANCELLED}
        )

        self.event.refresh_from_db()

        assert self.event.modification_number == (modification_number_before + 1)

    def test_sites_update_creates_new_modification(self):
        new_site = factories.SiteFactory.create()
        modification_number_before = self.event.modification_number

        services.update_event(
            event_id=self.event.id, data={}, selected_sites=[self.site1.id, new_site.id]
        )

        active_sites = models.SiteEvent.objects.filter(event_id=self.event.id).exclude(
            status=models.SiteEvent.EVENT_CANCELLED
        )

        assert self.event.modification_number == modification_number_before + 1
        assert active_sites.count() == 2

    def test_event_cancels_with_new_revision(self):
        # This should trigger a new modification
        modification_num_before = self.event.modification_number

        services.cancel_event(event_id=self.event.id)

        self.event.refresh_from_db()

        assert self.event.modification_number == (1 + modification_num_before)
        assert self.event.status == models.Event.EVENT_CANCELLED
