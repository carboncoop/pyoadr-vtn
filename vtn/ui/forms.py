from crispy_forms.helper import FormHelper
from crispy_forms.layout import Column
from crispy_forms.layout import Div
from crispy_forms.layout import HTML
from crispy_forms.layout import Layout
from crispy_forms.layout import Row
from crispy_forms.layout import Submit
from django import forms
from django.contrib.admin.widgets import AdminSplitDateTime
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import PasswordChangeForm
from django.core.exceptions import ValidationError
from django.utils import timezone

from vtn.models import Customer
from vtn.models import Event
from vtn.models import Program
from vtn.models import Site


class Login(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(Login, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            "username", "password", Submit("login", "Login", css_class="btn-primary")
        )


class PasswordChange(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            "old_password",
            "new_password1",
            "new_password2",
            Submit("change", "Change password", css_class="btn-primary"),
        )


class Customer(forms.ModelForm):
    class Meta:
        model = Customer
        fields = [
            "name",
            "external_id",
            "utility_id",
            "contact_name",
            "phone_number",
            "email_address",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-md-4"
        self.helper.field_class = "col-md-8"
        self.helper.layout = Layout(
            "name",
            "utility_id",
            "external_id",
            "contact_name",
            "phone_number",
            "email_address",
            Submit("submit", "Submit", css_class="btn btn-primary"),
        )


class Site(forms.ModelForm):
    class Meta:
        model = Site
        fields = [
            "site_name",
            "external_id",
            "site_location_code",
            "ip_address",
            "customer",
            "contact_name",
            "phone_number",
            "site_address1",
            "site_address2",
            "city",
            "state",
            "post_code",
            "ven_id",
            "ven_name",
            "programs",
        ]

    programs = forms.ModelMultipleChoiceField(
        queryset=Program.objects.all(),
        required=False,
        label="DR Programs",
        help_text="Hold down Control, or Cmd on a Mac, to select more than one.",
    )

    def __init__(self, *args, **kwargs):
        # This only applies if we're building from an existing instance
        if kwargs.get("instance"):
            # We get the 'initial' keyword argument or initialize it
            # as a dict if it didn't exist.
            initial = kwargs.setdefault("initial", {})
            # The widget for a ModelMultipleChoiceField expects
            # a list of primary keys for the selected data.
            initial["programs"] = [t.pk for t in kwargs["instance"].program_set.all()]

        super(Site, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        # Get the unsaved Site instance
        instance = forms.ModelForm.save(self, False)

        # Prepare a 'save_m2m' method for the form,
        old_save_m2m = self.save_m2m

        def save_m2m():
            old_save_m2m()
            # This is where we actually link the site with the programs
            instance.program_set.clear()
            instance.program_set.add(*self.cleaned_data["programs"])

        self.save_m2m = save_m2m

        # Do we need to save all changes now?
        if commit:
            instance.save()
            self.save_m2m()

        return instance

    @property
    def helper(self):
        helper = FormHelper()
        helper.form_class = "form-horizontal"
        helper.label_class = "col-lg-4"
        helper.field_class = "col-lg-8"
        helper.layout = Layout(
            Row(
                Column(
                    HTML("<div class='card mb-4'>"),
                    HTML("<h3 class='card-header'>Site information</h3>"),
                    HTML("<div class='card-body'>"),
                    "site_name",
                    "external_id",
                    "site_location_code",
                    "programs",
                    HTML("</div></div>"),
                    HTML("<div class='card mb'>"),
                    HTML("<h3 class='card-header'>Customer information</h3>"),
                    HTML("<div class='card-body'>"),
                    "customer",
                    "contact_name",
                    "phone_number",
                    HTML("</div></div>"),
                    css_class="col mr-2",
                ),
                Column(
                    HTML("<div class='card mb-4'>"),
                    HTML("<h3 class='card-header'>Location</h3>"),
                    HTML("<div class='card-body'>"),
                    "site_address1",
                    "site_address2",
                    "city",
                    "state",
                    "post_code",
                    HTML("</div></div>"),
                    HTML("<div class='card'>"),
                    HTML("<h3 class='card-header'>VEN</h3>"),
                    HTML("<div class='card-body'>"),
                    "ven_id",
                    "ven_name",
                    "ip_address",
                    HTML("</div></div>"),
                    css_class="col ml-2",
                ),
                css_class="form-row",
            ),
            Submit(
                "submit",
                "Submit",
                css_class="btn btn-primary btn-lg mx-auto d-block my-4",
            ),
        )
        return helper


class Event(forms.ModelForm):
    class Meta:
        model = Event
        fields = [
            "program",
            "scheduled_notification_time",
            "start",
            "end",
            "sites",
            "signal_payload",
        ]
        widgets = {"sites": FilteredSelectMultiple("sites", False)}

    scheduled_notification_time = forms.SplitDateTimeField(widget=AdminSplitDateTime())
    start = forms.SplitDateTimeField(widget=AdminSplitDateTime())
    end = forms.SplitDateTimeField(widget=AdminSplitDateTime())

    def __init__(self, *args, **kwargs):
        super(Event, self).__init__(*args, **kwargs)

        self.fields[
            "program"
        ].help_text = "Pick a program, and the sites below will update."

        self.fields[
            "signal_payload"
        ].help_text = """
            Please use the following integers:
            </br> 0 = Normal </br> 1 = Moderate </br> 2 = High </br> 3 = Special
            """

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(
                Div("program", "sites", "signal_payload", css_class="col pr-5"),
                Div(
                    "scheduled_notification_time", "start", "end", css_class="col pl-5"
                ),
                css_class="form-row",
            ),
            Submit(
                "save", "Save", css_class="btn btn-primary btn-lg mx-auto d-block my-4"
            ),
        )

    def clean(self):
        if self._errors:
            return self._errors
        cleaned_data = super().clean()

        try:
            scheduled_notification_time = cleaned_data["scheduled_notification_time"]
            start = cleaned_data["start"]
            end = cleaned_data["end"]
        except KeyError:
            raise ValidationError("Please enter valid date and time")

        if start > end:
            raise ValidationError("Start time must precede end time")

        if scheduled_notification_time > start:
            raise ValidationError("Notification time must precede start time")

        if start < timezone.now() or end < timezone.now():
            raise ValidationError("All times must be in the future")

        # If we are editing, don't check the scheduled notification time, because
        # we should be able to change an event after said time has passed.
        if not self.instance and scheduled_notification_time < timezone.now():
            raise ValidationError("All times must be in the future")


# To choose a customer from the DR Event detail screen
class EventCustomerDetail(forms.Form):

    customer = forms.ModelChoiceField(
        queryset=None, required=True, empty_label="--------- All ---------"
    )


# To choose a customer from the DR Event detail screen
class EventSiteDetail(forms.Form):

    site = forms.ModelChoiceField(
        queryset=None, required=True, empty_label="--------- All ---------"
    )


def ExportFilterHelper():
    helper = FormHelper()
    helper.form_method = "GET"
    helper.layout = Layout(
        Row(
            Column("program", css_class="flex-grow-1 ml-3"),
            Column("start", css_class="flex-grow-1 ml-3 pl-3 border-left"),
            css_class="d-flex",
        ),
        Submit(name="", value="Search"),
    )

    return helper
