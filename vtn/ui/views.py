from __future__ import unicode_literals

import csv
from collections import OrderedDict
from datetime import timedelta

import django.views.decorators.http as decorators
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Avg
from django.db.models import Case
from django.db.models import Count
from django.db.models import Min
from django.db.models import Q
from django.db.models import When
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.edit import ModelFormMixin
from django.views.generic.edit import UpdateView
from django_filters.views import FilterView

from . import filters
from . import forms
from vtn import models
from vtn import services
from vtn.tasks import update_event_statuses


class CustomerCreate(LoginRequiredMixin, CreateView):
    model = models.Customer
    form_class = forms.Customer
    template_name = "vtn/customer_create.html"
    success_url = reverse_lazy("vtn:home")


class CustomerDetail(LoginRequiredMixin, UpdateView):
    model = models.Customer
    form_class = forms.Customer
    template_name = "vtn/customer_detail.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerDetail, self).get_context_data(**kwargs)
        context["sites"] = self.object.site_set.all()
        return context


class SiteDetail(LoginRequiredMixin, UpdateView):
    model = models.Site
    form_class = forms.Site
    template_name = "vtn/site_detail.html"


class SiteCreate(LoginRequiredMixin, CreateView):
    model = models.Site
    form_class = forms.Site
    template_name = "vtn/site_create.html"

    def get_initial(self):
        customer = models.Customer.objects.get(pk=self.kwargs["pk"])
        return {"customer": customer}

    def get_success_url(self):
        return reverse_lazy("vtn:customer_detail", kwargs={"pk": self.kwargs["pk"]})

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if not self.object.ven_id:
            self.object.ven_id = services.get_new_ven_ID()
        self.object.save()

        return HttpResponseRedirect(
            reverse("vtn:customer_detail", kwargs={"pk": self.object.customer_id})
        )


@login_required
@decorators.require_GET
def cancel_event(request, pk):
    services.cancel_event(pk)
    return HttpResponseRedirect(reverse_lazy("vtn:home"))


@login_required
@decorators.require_GET
def export_csv(request, pk):
    event = get_object_or_404(models.Event, pk=pk)
    sites = models.Site.objects.filter(siteevent__event=event)

    t_data = (
        models.Telemetry.objects.filter(site__in=sites)
        .filter(reported_on__range=(event.start, event.end))
        .order_by("-reported_on")
    )

    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = f'attachment; filename="dr-data-event-{pk}.csv"'

    writer = csv.writer(response)

    writer.writerow(
        [
            "program_id",
            "site_id",
            "customer_id",
            "ven_id",
            "created_on",
            "reported_on",
            "report_specifier_id",
            "values",
        ]
    )
    for datum in t_data:
        writer.writerow(
            [
                event.program,
                datum.site.site_id,
                datum.site.customer.id,
                datum.site.ven_id,
                datum.created_on,
                datum.reported_on,
                datum.report_specifier_id,
                datum.values,
            ]
        )

    return response


class Export(LoginRequiredMixin, FilterView):
    filterset_class = filters.ExportFilter
    template_name = "vtn/export.html"

    # We set a queryset here to ensure annotations are in place before filtering starts
    queryset = (
        models.Event.objects.filter(deleted_at=None)
        .annotate(num_sites=Count("sites"))
        .select_related("program")
        .order_by("-start")
    )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["filter_helper"] = forms.ExportFilterHelper()
        return context


class EventCreate(LoginRequiredMixin, CreateView):
    model = models.Event
    form_class = forms.Event
    template_name = "vtn/event_create.html"

    def get_success_url(self):
        return reverse_lazy("vtn:home")

    def get_initial(self):
        initial = {
            "scheduled_notification_time": timezone.now() + timedelta(minutes=1),
            "start": timezone.now() + timedelta(minutes=5),
            "end": timezone.now() + timedelta(minutes=30),
        }
        return initial

    def form_valid(self, form):
        # Serialize the site list into a list of integer IDs to make the
        # boundary cleaner
        if "sites" in form.cleaned_data:
            sites = [site.id for site in form.cleaned_data.pop("sites")]
        else:
            sites = []

        services.create_event(form.cleaned_data, sites)

        return HttpResponseRedirect(self.get_success_url())


class EventUpdate(LoginRequiredMixin, UpdateView):
    model = models.Event
    form_class = forms.Event
    template_name = "vtn/event_update.html"

    def get_context_data(self, **kwargs):
        context = super(EventUpdate, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        # Serialize the site list into a list of integer IDs to make the
        # boundary cleaner
        if "sites" in form.cleaned_data:
            sites = [site.id for site in form.cleaned_data.pop("sites")]
        else:
            sites = []

        services.update_event(
            event_id=self.object.id,
            data=form.cleaned_data,
            selected_sites=sites,
            reason="Edit made in UI",
        )

        return super(ModelFormMixin, self).form_valid(form)


class Overview(LoginRequiredMixin, TemplateView):
    template_name = "vtn/home.html"

    def get_context_data(self, **kwargs):
        # XXX This should not be called here.
        update_event_statuses()

        context = {}

        context["customers"] = models.Customer.objects.annotate(
            sites=Count("site"),
            online=Count(Case(When(site__online=True, then=1))),
            offline=Count(Case(When(site__online=False, then=1))),
        ).order_by("name")

        # DR Event Table
        context["event_data"] = (
            models.Event.objects.filter(end__gt=timezone.now(), deleted_at=None)
            .annotate(numSites=Count("sites"))
            .select_related("program")
            .order_by("start")
        )

        return context


@login_required
def get_sites_for_program(request):
    program_id = request.GET.get("program", default="")
    if program_id == "":
        return JsonResponse([], safe=False)

    event_id = request.GET.get("event", default=None)
    if event_id == "":
        event_id = None

    program = get_object_or_404(models.Program, pk=program_id)

    if event_id:
        qs = program.sites.order_by("site_name").prefetch_related("siteevent_set")
        site_list = [
            {
                "id": site.id,
                "name": str(site),
                "status": (
                    site.siteevent_set.get(event_id=event_id).status
                    if site.siteevent_set.filter(event_id=event_id).count() > 0
                    else ""
                ),
                "selected": site.siteevent_set.filter(
                    ~Q(status="cancelled"), Q(event__id=event_id)
                ).count()
                > 0,
            }
            for site in qs
        ]

    else:
        site_list = [
            {"id": site.id, "name": str(site), "selected": False}
            for site in program.sites.order_by("site_name")
        ]

    return JsonResponse(site_list, safe=False)


@login_required
def get_event_details(request, pk):
    # This function is called when a customer or site is selected
    # on the DR Event detail screen. It loads the graph data for
    # the specified customer or site.

    customer = request.GET.get("customer", "")
    site_pk = request.GET.get("site", "")
    context = {}
    event = models.Event.objects.get(pk=pk)

    if customer != "":
        if customer == "empty":
            sites = models.Site.objects.filter(siteevent__event=event)
        else:
            customers = models.Customer.objects.all().order_by("pk")
            customer = customers.get(pk=customer)
            sites = models.Site.objects.filter(siteevent__event=event).filter(
                customer=customer
            )
    elif site_pk == "empty":
        sites = models.Site.objects.filter(siteevent__event=event)
    else:
        sites = models.Site.objects.filter(pk=site_pk)

    start = event.start
    end = event.end

    date_slice = "trunc(extract(epoch from created_on) / '{}' ) * {}".format(
        str(settings.GRAPH_TIMECHUNK_SECONDS), str(settings.GRAPH_TIMECHUNK_SECONDS)
    )
    t_data = (
        models.Telemetry.objects.filter(site__in=sites)
        .filter(created_on__range=(start, end))
        .extra(select={"date_slice": date_slice})
        .values("date_slice", "site")
        .annotate(
            avg_baseline_power_kw=Avg("baseline_power_kw"),
            avg_measured_power_kw=Avg("measured_power_kw"),
            time=Min("created_on"),
        )
    )
    if t_data.count() == 0:
        context["no_data_for_sites"] = "True"
        return render(request, "vtn/event_customer_detail.html", context)
    else:
        co = t_data.order_by("-created_on")
        context["t_data"] = t_data
        last = co.first()["time"]
        first = co.last()["time"]
        difference = (last - first).seconds
        quarter = difference // 4
        last = last - timedelta(seconds=quarter)
        first = first + timedelta(seconds=quarter)
        context["start_focus"] = first
        context["end_focus"] = last

        sum_baseline = {}
        sum_measured = {}
        for datum in t_data:
            if datum["date_slice"] in sum_baseline:
                sum_baseline[datum["date_slice"]] += datum["avg_baseline_power_kw"]
            else:
                sum_baseline[datum["date_slice"]] = datum["avg_baseline_power_kw"]
            if datum["date_slice"] in sum_measured:
                sum_measured[datum["date_slice"]] += datum["avg_measured_power_kw"]
            else:
                sum_measured[datum["date_slice"]] = datum["avg_measured_power_kw"]

        context["sum_baseline"] = OrderedDict(
            sorted(sum_baseline.items(), key=lambda t: t[0])
        )
        context["sum_measured"] = OrderedDict(
            sorted(sum_measured.items(), key=lambda t: t[0])
        )
        context["no_data_for_sites"] = "False"

        return render(request, "vtn/event_customer_detail.html", context)


@login_required
def event_view(request, pk):
    # This function is called after a user clicks on a DR Event on the
    # overview screen. It redirects the request to either the DR Event
    # update screen or DR Event detail screen, depending on whether the
    # event's start time has passed.

    event = get_object_or_404(models.Event, pk=pk)
    if event.start < timezone.now():
        path = "vtn:event_detail"
    else:
        path = "vtn:event_update"

    return HttpResponseRedirect(reverse(path, kwargs={"pk": pk}))


class EventDetail(LoginRequiredMixin, TemplateView):
    template_name = "vtn/event_detail.html"

    def get_context_data(self, **kwargs):
        context = super(EventDetail, self).get_context_data(**kwargs)
        event = models.Event.objects.get(pk=self.kwargs["pk"])
        sites = models.Event.objects.get(pk=self.kwargs["pk"]).sites.all()
        customer_form = forms.EventCustomerDetail()
        site_form = forms.EventSiteDetail()

        # Fill out context fields
        customer_form.fields["customer"].queryset = models.Customer.objects.filter(
            site__in=sites
        ).distinct()
        site_form.fields["site"].queryset = models.Site.objects.filter(
            siteevent__event=event
        )
        context["event"] = event
        context["customerForm"] = customer_form
        context["siteForm"] = site_form
        context["status"] = get_status(event)
        context["pk"] = self.kwargs["pk"]
        context["start"] = event.start
        context["end"] = event.end

        # Get site events for "Site Detail" tab
        context["site_events"] = models.SiteEvent.objects.filter(event=event)
        site_events = models.SiteEvent.objects.filter(event=event)

        for site_event in site_events:
            site_event.last_stat = get_most_recent_stat(event, site_event.site)

        context["site_events"] = site_events

        # Only get those sites that have a corresponding Site Event
        sites = models.Site.objects.filter(siteevent__event=event)

        # If there is no telemetry, tell template there is none so 'No data' is displayed
        if (
            models.Telemetry.objects.filter(site__in=sites)
            .filter(created_on__range=(event.start, event.end))
            .count()
            == 0
        ):
            context["no_data"] = True

        # If there is telemetry...
        else:
            start = event.start
            end = event.end

            date_slice = "trunc(extract(epoch from created_on) / '{}' ) * {}".format(
                str(settings.GRAPH_TIMECHUNK_SECONDS),
                str(settings.GRAPH_TIMECHUNK_SECONDS),
            )
            t_data = (
                models.Telemetry.objects.filter(site__in=sites)
                .filter(created_on__range=(start, end))
                .extra(select={"date_slice": date_slice})
                .values("date_slice", "site")
                .annotate(
                    avg_baseline_power_kw=Avg("baseline_power_kw"),
                    avg_measured_power_kw=Avg("measured_power_kw"),
                    time=Min("created_on"),
                )
            )

            co = t_data.order_by("-created_on")
            context["t_data"] = t_data
            last = co.first()["time"]
            first = co.last()["time"]
            difference = (last - first).seconds
            quarter = difference // 4
            last = last - timedelta(seconds=quarter)
            first = first + timedelta(seconds=quarter)
            context["start_focus"] = first
            context["end_focus"] = last

            sum_baseline = {}
            sum_measured = {}
            for datum in t_data:
                if datum["date_slice"] in sum_baseline:
                    sum_baseline[datum["date_slice"]] += datum["avg_baseline_power_kw"]
                else:
                    sum_baseline[datum["date_slice"]] = datum["avg_baseline_power_kw"]
                if datum["date_slice"] in sum_measured:
                    sum_measured[datum["date_slice"]] += datum["avg_measured_power_kw"]
                else:
                    sum_measured[datum["date_slice"]] = datum["avg_measured_power_kw"]

            context["sum_baseline"] = OrderedDict(
                sorted(sum_baseline.items(), key=lambda t: t[0])
            )
            context["sum_measured"] = OrderedDict(
                sorted(sum_measured.items(), key=lambda t: t[0])
            )

        return context


def get_most_recent_stat(event, site):
    """
    :param site: The site to get the most recent measured power stat for.
    :param event: Used to get start and end times for telemetry.
    :return: Ideally, returns the difference between the site's baseline power
             and its actual power. If there is no baseline, it returns 'N.A.
    """
    try:
        t_data = (
            models.Telemetry.objects.filter(site=site)
            .filter(reported_on__range=(event.start, event.end))
            .order_by("-reported_on")[0]
        )
        if t_data.baseline_power_kw is not None:
            return t_data.baseline_power_kw - t_data.measured_power_kw
        else:
            return t_data.measured_power_kw
    except (IndexError, Exception):
        return "N.A."


def get_status(event):
    if event.start < timezone.now():
        return "Active"
    elif event.scheduled_notification_time < timezone.now():
        return "Notification"
