import django_filters as filters
from django_filters import widgets

from vtn import models


class DateRangeWidget(widgets.RangeWidget):
    template_name = "widgets/datemultiwidget.html"


class ExportFilter(filters.FilterSet):
    class Meta:
        model = models.Event
        fields = ["start", "program"]

    program = filters.ModelChoiceFilter(queryset=models.Program.objects)
    start = filters.DateFromToRangeFilter(
        label="Event start date (between)",
        widget=DateRangeWidget(attrs={"type": "date", "class": "form-control"}),
    )
