from django.contrib.auth import views as auth_views
from django.urls import path
from django.urls import reverse_lazy

from . import forms
from . import views

app_name = "vtn.ui"

urlpatterns = [
    path("", views.Overview.as_view(), name="home"),
    path(
        "login/",
        auth_views.LoginView.as_view(
            template_name="login.html",
            authentication_form=forms.Login,
            redirect_authenticated_user=True,
        ),
        name="login",
    ),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path(
        "password_change/",
        auth_views.PasswordChangeView.as_view(
            template_name="vtn/change_password.html",
            form_class=forms.PasswordChange,
            success_url=reverse_lazy("vtn:home"),
        ),
        name="change_password",
    ),
    path("customer/add", views.CustomerCreate.as_view(), name="customer_add"),
    path("customer/<slug:pk>", views.CustomerDetail.as_view(), name="customer_detail"),
    path("site/create/<int:pk>", views.SiteCreate.as_view(), name="create_site"),
    path("site/<slug:pk>", views.SiteDetail.as_view(), name="site_detail"),
    path("export", views.Export.as_view(), name="export"),
    path("export/<int:pk>.csv", views.export_csv, name="export_events_csv"),
    path("program/sites", views.get_sites_for_program, name="get_sites_for_program"),
    path("event/create", views.EventCreate.as_view(), name="event"),
    path("event/<int:pk>", views.event_view, name="event_dispatch"),
    path("event/<int:pk>/cancel", views.cancel_event, name="event_cancel"),
    path("event/<int:pk>/view", views.EventDetail.as_view(), name="event_detail"),
    path(
        "event/<int:pk>/view/customer",
        views.get_event_details,
        name="event_get_details",
    ),
    path("event/<int:pk>/update", views.EventUpdate.as_view(), name="event_update"),
]
