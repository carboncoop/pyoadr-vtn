from datetime import timedelta

from django.conf import settings
from django.utils import timezone
from django_rq import job

from vtn import services
from vtn.models import Event
from vtn.models import Site


@job
def update_event_statuses():
    now = timezone.now()
    events = Event.objects.exclude(status="completed").exclude(status="cancelled")
    for event in events:
        if event.end < now:
            services.update_event(event.id, {"status": "completed"})
        elif event.start < now:
            services.update_event(event.id, {"status": "active"})
    return


@job
def update_online_offline():

    now = timezone.now()
    sites = Site.objects.all()

    for site in sites:
        try:
            if (
                site.last_status_time
                + timedelta(minutes=settings.ONLINE_INTERVAL_MINUTES)
                < now
            ):
                site.online = False
                site.save()
            else:
                site.online = True
                site.save()
        except TypeError:
            continue
