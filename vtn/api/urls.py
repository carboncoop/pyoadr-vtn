from django.urls import include
from django.urls import path
from django.views.generic import TemplateView
from rest_framework import routers
from rest_framework.schemas import get_schema_view

from . import views

app_name = "vtn.api"


router = routers.DefaultRouter()
router.register(r"sites", views.SiteViewSet)
router.register(r"customers", views.CustomerViewSet)
router.register(r"programs", views.ProgramViewSet)
router.register(r"events", views.EventViewSet)

schema_url_patterns = [
    path("api/", include("vtn.api.urls")),
]

urlpatterns = [
    path("v1/", include(router.urls)),
    path("v1/events/", views.EventList.as_view()),
    # path("v1/events/<int:event_pk>/", views.Event.as_view()),
    path(
        "v1/sites/<int:pk>/generate_cert/",
        views.GenerateSiteCertificate.as_view(),
        name="site-generate-cert",
    ),
    path("v1/sites/<int:site_pk>/events/", views.SiteEvents.as_view()),
    path(
        "v1/sites/<int:site_pk>/events/<int:event_id>/opt_out",
        views.OptOut.as_view(),
        name="site-event-opt-out",
    ),
    path(
        "v1/sites/<int:site_pk>/events/<int:event_id>/opt_back_in",
        views.OptBackIn.as_view(),
        name="site-event-opt-back-in",
    ),
    path("ca/certificate", views.ca_certificate, name="ca-certificate"),
    path("ca/crl", views.ca_crl, name="ca-crl"),
    path(
        "openapi",
        get_schema_view(
            title="PyOADR-VTN",
            description="Management API",
            patterns=schema_url_patterns,
        ),
        name="openapi-schema",
    ),
    path(
        "docs/",
        TemplateView.as_view(
            template_name="docs.html",
            extra_context={"schema_url": "api:openapi-schema"},
        ),
        name="api-docs",
    ),
]
