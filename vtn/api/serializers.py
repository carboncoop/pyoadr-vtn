from rest_framework import serializers

from vtn.models import Customer
from vtn.models import Event
from vtn.models import Program
from vtn.models import Site


class SiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = (
            "id",
            "customer",
            "site_name",
            "site_id",
            "external_id",
            "ven_id",
            "ven_name",
            "site_location_code",
            "ip_address",
            "site_address1",
            "site_address2",
            "city",
            "state",
            "post_code",
            "contact_name",
            "phone_number",
            "online",
            "last_status_time",
        )


class EnrolSiteInProgram(serializers.Serializer):
    program_id = serializers.IntegerField()


class CustomerSerializer(serializers.ModelSerializer):
    sites = SiteSerializer(many=True, read_only=True)

    class Meta:
        model = Customer
        fields = (
            "id",
            "name",
            "utility_id",
            "contact_name",
            "phone_number",
            "external_id",
            "sites",
        )


class ProgramSerializer(serializers.ModelSerializer):
    sites = SiteSerializer(many=True, read_only=True)

    class Meta:
        model = Program
        fields = ("id", "name", "sites")


class SiteEventSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    event_id = serializers.IntegerField(required=True)
    program_id = serializers.IntegerField(required=True, source="event.program_id")
    start = serializers.DateTimeField(required=True, source="event.start")
    end = serializers.DateTimeField(required=True, source="event.end")
    status = serializers.CharField(required=True)
    event_status = serializers.CharField(required=True, source="event.status")


class NestedSiteEventSerializer(serializers.Serializer):
    id = serializers.PrimaryKeyRelatedField(read_only=True, source="site")
    status = serializers.CharField(required=True)


class EventSerializer(serializers.ModelSerializer):
    sites = NestedSiteEventSerializer(many=True, read_only=True, source="siteevent_set")

    class Meta:
        model = Event
        fields = (
            "id",
            "program_id",
            "scheduled_notification_time",
            "start",
            "end",
            "status",
            "signal_payload",
            "sites",
        )


class GenerateCertInput(serializers.Serializer):
    replace = serializers.BooleanField(required=False, default=False)
