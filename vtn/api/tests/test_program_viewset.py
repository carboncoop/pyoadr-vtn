from django.shortcuts import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_api_key.models import APIKey

from vtn.openadr.tests.factories import ProgramFactory


class ProgramGetTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_get_derprogram_list(self):
        ProgramFactory(name="first_dr")
        ProgramFactory(name="second_dr")

        response = self.client.get(reverse("api:program-list"), format="json")
        assert response.status_code == status.HTTP_200_OK
        assert response.data[0]["name"] == "first_dr"
        assert response.data[1]["name"] == "second_dr"

    def test_get_derprogram(self):
        program = ProgramFactory()
        response = self.client.get(
            reverse("api:program-detail", kwargs={"pk": program.id}), format="json"
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.data["name"] == program.name
