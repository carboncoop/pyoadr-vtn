from django.conf import settings
from django.urls import reverse
from django_x509.models import Ca
from django_x509.models import Cert
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_api_key.models import APIKey

from vtn.models import Site
from vtn.models import SiteCertificate
from vtn.openadr.tests.factories import CustomerFactory
from vtn.openadr.tests.factories import ProgramFactory
from vtn.openadr.tests.factories import SiteFactory


class SiteViewSetListTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key
        Ca.objects.create(name=settings.CA_NAME)

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_get_empty_site_list(self):
        response = self.client.get(reverse("api:site-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        assert response.data == []

    def test_get_non_empty_site_list(self):
        customer = CustomerFactory()
        site = SiteFactory(customer=customer)
        response = self.client.get(reverse("api:site-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        assert len(response.data) == 1
        assert response.data[0] == {
            "id": site.id,
            "customer": customer.id,
            "site_name": site.site_name,
            "site_id": str(site.site_id),
            "external_id": str(site.external_id),
            "ven_id": str(site.ven_id),
            "ven_name": site.ven_name,
            "site_location_code": str(site.site_location_code),
            "ip_address": site.ip_address,
            "site_address1": site.site_address1,
            "site_address2": site.site_address2,
            "city": site.city,
            "state": site.state,
            "post_code": site.post_code,
            "contact_name": site.contact_name,
            "phone_number": str(site.phone_number),
            "online": site.online,
            "last_status_time": site.last_status_time.replace(tzinfo=None).isoformat()
            + "Z",
        }


class SiteViewSetGetTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key
        Ca.objects.create(name=settings.CA_NAME)

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_get_site(self):
        customer = CustomerFactory()
        site = SiteFactory(customer=customer)
        response = self.client.get(
            reverse("api:site-detail", kwargs={"pk": site.id}), format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        assert response.data == {
            "id": site.id,
            "customer": customer.id,
            "site_name": site.site_name,
            "site_id": str(site.site_id),
            "external_id": str(site.external_id),
            "ven_id": str(site.ven_id),
            "ven_name": site.ven_name,
            "site_location_code": str(site.site_location_code),
            "ip_address": site.ip_address,
            "site_address1": site.site_address1,
            "site_address2": site.site_address2,
            "city": site.city,
            "state": site.state,
            "post_code": site.post_code,
            "contact_name": site.contact_name,
            "phone_number": str(site.phone_number),
            "online": site.online,
            "last_status_time": site.last_status_time.replace(tzinfo=None).isoformat()
            + "Z",
        }


class SiteViewSetCreateTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key
        Ca.objects.create(name=settings.CA_NAME)

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_post_site(self):
        customer = CustomerFactory()
        original_num_sites = Site.objects.count()
        fake = Faker()
        data = {
            "customer": customer.id,
            "site_name": fake.bothify(),
            "external_id": fake.random_digit(),
            "ven_id": fake.random_digit(),
            "ven_name": fake.bothify(),
            "site_location_code": fake.locale(),
            "ip_address": fake.ipv4_private(network=False, address_class=None),
            "site_address1": fake.street_address(),
            "site_address2": fake.street_name(),
            "city": fake.city(),
            "state": fake.state_abbr(),
            "post_code": fake.zipcode(),
            "contact_name": fake.name(),
            "phone_number": fake.msisdn(),
        }

        response = self.client.post(reverse("api:site-list"), data, format="json")
        assert response.status_code == status.HTTP_201_CREATED
        assert Site.objects.count() == original_num_sites + 1
        assert response.data["customer"] == customer.id
        assert "site_id" in response.data
        assert response.data["site_name"] == data["site_name"]
        assert response.data["external_id"] == str(data["external_id"])
        assert response.data["ven_id"] == str(data["ven_id"])
        assert response.data["ven_name"] == data["ven_name"]
        assert response.data["site_location_code"] == data["site_location_code"]
        assert response.data["ip_address"] == data["ip_address"]
        assert response.data["site_address1"] == data["site_address1"]
        assert response.data["site_address2"] == data["site_address2"]
        assert response.data["city"] == data["city"]
        assert response.data["state"] == data["state"]
        assert response.data["post_code"] == data["post_code"]
        assert response.data["contact_name"] == data["contact_name"]
        assert response.data["phone_number"] == data["phone_number"]
        assert response.data["last_status_time"] is None
        assert response.data["online"] is False
        assert len(response.data) == 18

    def test_site_cannot_be_created_with_customer_id_of_non_existant_customer(self):
        original_num_sites = Site.objects.count()
        fake = Faker()
        data = {
            "customer": 100,
            "site_name": fake.bothify(),
            "external_id": fake.random_digit(),
            "ven_id": fake.random_digit(),
            "ven_name": fake.bothify(),
            "site_location_code": fake.locale(),
            "ip_address": fake.ipv4_private(network=False, address_class=None),
            "site_address1": fake.street_address(),
            "site_address2": fake.street_name(),
            "city": fake.city(),
            "state": fake.state_abbr(),
            "post_code": fake.zipcode(),
            "contact_name": fake.name(),
            "phone_number": fake.msisdn(),
        }
        response = self.client.post(reverse("api:site-list"), data, format="json")

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert Site.objects.count() == original_num_sites

    def test_site_cannot_be_created_with_no_customer(self):
        original_num_sites = Site.objects.count()
        fake = Faker()
        data = {
            "site_name": fake.bothify(),
            "external_id": fake.random_digit(),
            "ven_id": fake.random_digit(),
            "ven_name": fake.bothify(),
            "site_location_code": fake.locale(),
            "ip_address": fake.ipv4_private(network=False, address_class=None),
            "site_address1": fake.street_address(),
            "site_address2": fake.street_name(),
            "city": fake.city(),
            "state": fake.state_abbr(),
            "post_code": fake.zipcode(),
            "contact_name": fake.name(),
            "phone_number": fake.msisdn(),
        }
        response = self.client.post(reverse("api:site-list"), data, format="json")
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert Site.objects.count() == original_num_sites


class GenerateCertTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        Ca.objects.create(name=settings.CA_NAME)
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key
        customer = CustomerFactory()
        cls.site1 = SiteFactory(customer=customer)

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_cert_create_endpoint(self):
        num_site_certs = SiteCertificate.objects.count()
        num_certs = Cert.objects.count()
        response = self.client.post(
            reverse("api:site-generate-cert", kwargs={"pk": self.site1.pk})
        )

        cert = Cert.objects.last()
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data == {"pem_bundle": cert.private_key + cert.certificate}
        assert SiteCertificate.objects.count() == num_site_certs + 1
        assert Cert.objects.count() == num_certs + 1

    def test_cert_create_endpoint_called_again_with_replace(self):
        num_site_certs = SiteCertificate.objects.count()
        num_certs = Cert.objects.count()
        url = reverse("api:site-generate-cert", kwargs={"pk": self.site1.pk})

        response = self.client.post(url, {"replace": True}, format="json")
        response = self.client.post(url, {"replace": True}, format="json")

        assert response.status_code == status.HTTP_201_CREATED
        assert SiteCertificate.objects.count() == num_site_certs + 1
        assert Cert.objects.count() == num_certs + 1


class EnrollSiteinProgram(APITestCase):
    @classmethod
    def setUpTestData(cls):
        Ca.objects.create(name=settings.CA_NAME)
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key
        customer = CustomerFactory()
        cls.site1 = SiteFactory(customer=customer)
        cls.program = ProgramFactory()

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_add_site_to_der_program(self):
        number_of_site_in_program = self.program.sites.count()
        data = {"program_id": self.program.id}
        response = self.client.put(
            reverse("api:site-enrol-in-program", kwargs={"pk": self.site1.pk}),
            data,
            format="json",
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert self.program.sites.count() == number_of_site_in_program + 1
        assert self.program.sites.first() == self.site1
