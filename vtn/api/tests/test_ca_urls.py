from django import urls
from django.conf import settings
from django.test import TestCase
from django_x509.models import Ca
from faker import Faker

from vtn.api import views

fake = Faker()


class CACRLTest(TestCase):
    def test_url(self):
        match = urls.resolve("/api/ca/crl")

        assert match.func == views.ca_crl

    def test_url_name(self):
        assert urls.reverse("api:ca-crl") == "/api/ca/crl"

    def test_it_works(self):
        settings.CA_NAME = fake.name()
        ca = Ca.objects.create(name=settings.CA_NAME)

        response = views.ca_crl(None)

        assert response["content-type"] == "application/x-pem-file"
        assert response.content == ca.crl


class CACertificateTest(TestCase):
    def test_url(self):
        match = urls.resolve("/api/ca/certificate")

        assert match.func == views.ca_certificate

    def test_url_name(self):
        assert urls.reverse("api:ca-certificate") == "/api/ca/certificate"

    def test_it_works(self):
        settings.CA_NAME = fake.name()
        ca = Ca.objects.create(name=settings.CA_NAME)

        response = views.ca_certificate(None)

        assert response["content-type"] == "application/x-pem-file"
        assert response.content.decode() == ca.certificate
