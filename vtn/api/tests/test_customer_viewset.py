import pytest
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_api_key.models import APIKey

from vtn.models import Customer
from vtn.openadr.tests.factories import CustomerFactory


class CustomerViewSetListTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_get_empty_customer_list(self):
        response = self.client.get(reverse("api:customer-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        assert response.data == []

    def test_get_non_empty_customer_list(self):
        customer = CustomerFactory()
        response = self.client.get(reverse("api:customer-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        assert len(response.data) == 1
        assert response.data[0] == {
            "id": customer.id,
            "name": customer.name,
            "utility_id": str(customer.utility_id),
            "external_id": customer.external_id,
            "contact_name": customer.contact_name,
            "phone_number": str(customer.phone_number),
            "sites": [],
        }


class CustomerViewSetGetTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_get_customer(self):
        customer = CustomerFactory()
        response = self.client.get(
            reverse("api:customer-detail", kwargs={"pk": customer.id}), format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        assert response.data == {
            "id": customer.id,
            "name": customer.name,
            "utility_id": str(customer.utility_id),
            "external_id": customer.external_id,
            "contact_name": customer.contact_name,
            "phone_number": str(customer.phone_number),
            "sites": [],
        }


class CustomerViewSetCreateTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_post_customer(self):
        original_num_customers = Customer.objects.count()
        fake = Faker()
        data = {
            "name": fake.name(),
            "external_id": fake.name(),
            "utility_id": fake.ean13(),
            "contact_name": fake.name(),
            "phone_number": fake.msisdn(),
        }
        response = self.client.post(reverse("api:customer-list"), data, format="json")
        assert response.status_code == status.HTTP_201_CREATED
        Customer.objects.count() == original_num_customers + 1
        assert len(response.data) == 7
        assert response.data["name"] == data["name"]
        assert response.data["external_id"] == data["external_id"]
        assert response.data["utility_id"] == str(data["utility_id"])
        assert response.data["contact_name"] == str(data["contact_name"])
        assert response.data["phone_number"] == str(data["phone_number"])

    def test_customer_not_created_if_no_name(self):
        original_num_customers = Customer.objects.count()
        fake = Faker()
        data = {
            "external_id": fake.name(),
            "utility_id": fake.ean13(),
            "contact_name": fake.name(),
            "phone_number": fake.msisdn(),
        }
        response = self.client.post(reverse("api:customer-list"), data, format="json")
        assert response.json()["name"][0] == "This field is required."
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        Customer.objects.count() == original_num_customers


class CustomerViewSetDeleteTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        api_key, generated_key = APIKey.objects.create_key(name="Backend API")
        cls.generated_key = generated_key
        cls.customer1 = CustomerFactory()
        cls.customer2 = CustomerFactory()

    def setUp(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Api-Key {self.generated_key}")

    def test_delete_customer(self):
        original_num_customers = Customer.objects.count()
        self.client.delete(
            reverse("api:customer-detail", kwargs={"pk": self.customer1.id}),
            format="json",
        )
        assert Customer.objects.count() == original_num_customers - 1
        with pytest.raises(ObjectDoesNotExist):
            Customer.objects.get(pk=self.customer1.pk)
