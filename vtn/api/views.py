from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django_x509.models import Ca
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView
from rest_framework.parsers import FormParser
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.viewsets import ViewSet
from rest_framework_api_key.permissions import HasAPIKey

from . import serializers
from vtn import exceptions as vtn_exceptions
from vtn import services
from vtn.const import ALL
from vtn.enums import EventStatus
from vtn.models import Customer
from vtn.models import Event
from vtn.models import Program
from vtn.models import Site
from vtn.models import SiteEvent


def get_ca_or_500():
    try:
        return Ca.objects.get(name=settings.CA_NAME)
    except Ca.DoesNotExist:
        raise Exception(
            f"Server misconfigured; CA with name '{settings.CA_NAME}' not found"
        )


class SiteViewSet(ModelViewSet):
    permission_classes = [HasAPIKey | IsAuthenticated]
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    parser_classes = (JSONParser, FormParser)
    queryset = Site.objects.all()
    serializer_class = serializers.SiteSerializer
    filter_backends = (SearchFilter,)
    search_fields = ("ven_id",)

    @action(detail=True, methods=["put"])
    def enrol_in_program(self, request, pk=None):
        site = self.get_object()
        serializer = serializers.EnrolSiteInProgram(request.data)
        program = get_object_or_404(Program, pk=serializer.data["program_id"])
        program.sites.add(site)
        program.save()
        return Response(
            {"Success": f"Enroled site {site.id} in program {program.id}"},
            status=status.HTTP_204_NO_CONTENT,
        )


class GenerateSiteCertificate(APIView):
    permission_classes = [HasAPIKey | IsAuthenticated]
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    parser_classes = (JSONParser, FormParser)

    def post(self, request, pk=None):
        serializer = serializers.GenerateCertInput(data=request.data)
        serializer.is_valid(raise_exception=True)

        site = get_object_or_404(Site, pk=pk)
        default_ca = get_ca_or_500()

        site_cert = services.generate_cert(
            default_ca, site, **serializer.validated_data
        )

        return Response(
            {"pem_bundle": site_cert.cert.private_key + site_cert.cert.certificate},
            status=status.HTTP_201_CREATED,
        )


class CustomerViewSet(ModelViewSet):
    permission_classes = [HasAPIKey | IsAuthenticated]
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    parser_classes = (JSONParser, FormParser)
    queryset = Customer.objects.all()
    serializer_class = serializers.CustomerSerializer
    filter_backends = (SearchFilter,)
    search_fields = ("external_id",)


class ProgramViewSet(ViewSet):
    permission_classes = [HasAPIKey | IsAuthenticated]
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    parser_classes = (JSONParser, FormParser)
    serializer_class = serializers.ProgramSerializer
    queryset = Program.objects.all()

    def list(self, request):
        serializer = serializers.ProgramSerializer(self.queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        program = get_object_or_404(self.queryset, pk=pk)
        serializer = serializers.ProgramSerializer(program)
        return Response(serializer.data)


class EventViewSet(ViewSet):
    permission_classes = [HasAPIKey | IsAuthenticated]
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    parser_classes = (JSONParser, FormParser)
    serializer_class = serializers.EventSerializer
    queryset = Event.objects.all()

    def retrieve(self, request, pk=None):
        event = get_object_or_404(self.queryset, pk=pk)
        serializer = serializers.EventSerializer(event)
        return Response(serializer.data)


class EventList(ListAPIView):
    """Returns a list of Events

    By default, the `/site/{id}/events/` endpoint will return upcoming and pending events.
    Other EventStatuses can be called by adding the parameter `?status=cancelled/completed/all`.

    Events can be date filtered using the `since=` parameter.
    This will return events with a start date greater or equal to the date parameter specified.
    Dates should be in ISO format, eg `&since=2019-12-03T10:54`
    """

    permission_classes = [HasAPIKey | IsAuthenticated]
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    parser_classes = (JSONParser, FormParser)
    serializer_class = serializers.EventSerializer

    def get_queryset(self):
        queryset = Event.objects.filter(deleted_at=None)
        status = self.request.query_params.get("status", None)
        since = self.request.query_params.get("since", None)
        if status in EventStatus.list():
            queryset = queryset.filter(status=status)
        elif status == ALL:
            pass
        else:
            queryset = queryset.exclude(status=EventStatus.CANCELLED.value)
            queryset = queryset.exclude(status=EventStatus.COMPLETED.value)
        if since:
            queryset = queryset.filter(start__gte=since)
        return queryset


class SiteEvents(ListAPIView):
    """Returns a list of Events associated with a Site. The SiteEvent status will be
    returned for each Event. Events will be returned in chronological order, earliest
    first.

    By default, the `/site/{id}/events/` endpoint will return upcoming & pending events.

    Other EventStatuses can be called by adding the parameter `?status=cancelled/completed/all`.
    """

    permission_classes = [HasAPIKey | IsAuthenticated]
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    parser_classes = (JSONParser, FormParser)
    serializer_class = serializers.SiteEventSerializer

    def get_queryset(self):
        site_pk = self.kwargs["site_pk"]
        queryset = (
            SiteEvent.objects.filter(site__id=site_pk)
            .exclude(event=None)
            .order_by("event__start")
        )
        status = self.request.query_params.get("status", None)
        if status in EventStatus.list():
            queryset = queryset.filter(event__status=status)
        elif status == ALL:
            pass
        else:
            queryset = queryset.exclude(event__status=EventStatus.CANCELLED.value)
            queryset = queryset.exclude(event__status=EventStatus.COMPLETED.value)

        return queryset


def ca_certificate(request):
    ca = get_ca_or_500()
    response = HttpResponse(
        ca.certificate, status=200, content_type="application/x-pem-file"
    )
    response["Content-Disposition"] = 'attachment; filename="cert.crt"'
    return response


def ca_crl(request):
    ca = get_ca_or_500()
    response = HttpResponse(ca.crl, status=200, content_type="application/x-pem-file")
    response["Content-Disposition"] = 'attachment; filename="crl.pem"'
    return response


class OptOut(APIView):
    permission_classes = [HasAPIKey | IsAuthenticated]
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    parser_classes = (JSONParser, FormParser)

    def get(self, request, site_pk, event_id, format=None):
        try:
            services.remove_site_from_event(event_id, site_pk)
            return Response(
                {"detail": f"Opted site {site_pk} out of event {event_id}"},
                status=status.HTTP_200_OK,
            )
        except Event.DoesNotExist:
            return Response({"detail": "Not Found"}, status=status.HTTP_404_NOT_FOUND)


class OptBackIn(APIView):
    """Opt a site back into an event from which it has previously opted out."""

    permission_classes = [HasAPIKey | IsAuthenticated]
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    parser_classes = (JSONParser, FormParser)

    def get(self, request, site_pk, event_id, format=None):
        try:
            services.resubscribe_site_to_event(event_id, site_pk)
        except Event.DoesNotExist:
            return Response(
                {"detail": "Event not found"}, status=status.HTTP_404_NOT_FOUND
            )
        except vtn_exceptions.NotOptedOut:
            return Response(
                {"detail": "Site has not opted out"}, status=status.HTTP_404_NOT_FOUND
            )
        else:
            return Response(
                {"detail": f"Opted site {site_pk} back into event {event_id}"},
                status=status.HTTP_200_OK,
            )
