from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = "vtn.api"
