from __future__ import unicode_literals

from django.contrib import admin
from django.forms.models import BaseInlineFormSet

from . import models


@admin.register(models.SiteCertificate)
class SiteCertificateAdmin(admin.ModelAdmin):
    list_display = ("site", "cert", "ven_id", "certificate_sha1_fingerprint")

    def ven_id(self, obj) -> str:
        return obj.site.ven_id


@admin.register(models.Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ("name", "utility_id")
    list_per_page = 10


class TelemetryFormSet(BaseInlineFormSet):
    def get_queryset(self):
        qs = super(TelemetryFormSet, self).get_queryset()
        return qs[:5]


class TelemetryInline(admin.TabularInline):
    model = models.Telemetry
    fk_name = "site"
    extra = 0
    formset = TelemetryFormSet
    classes = {"collapse"}
    fields = (
        "created_on",
        "reported_on",
        "report_specifier_id",
        "values",
    )

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class SiteEventInline(admin.TabularInline):
    model = models.SiteEvent
    fk_name = "site"
    extra = 0

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class EventRevisionsInline(admin.TabularInline):
    model = models.SupersededEvent
    readonly_fields = (
        "program",
        "scheduled_notification_time",
        "start",
        "end",
        "status",
        "created_on",
    )
    fk_name = "event"
    extra = 0

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.Site)
class SiteAdmin(admin.ModelAdmin):
    list_display = (
        "customer",
        "site_name",
        "site_id",
        "site_location_code",
        "ip_address",
        "site_address1",
        "site_address2",
        "city",
        "state",
        "post_code",
        "contact_name",
        "phone_number",
        "online",
        "ven_id",
        "ven_name",
    )

    fields = (
        "customer",
        "site_id",
        "site_name",
        "external_id",
        "site_location_code",
        "ip_address",
        "site_address1",
        "site_address2",
        "city",
        "state",
        "post_code",
        "contact_name",
        "phone_number",
        "online",
        "ven_id",
        "ven_name",
    )

    list_per_page = 10
    inlines = (TelemetryInline, SiteEventInline)

    def get_readonly_fields(self, request, obj=None):
        return ["site_id", "ven_id", "online", "last_status_time"]


@admin.register(models.Program)
class ProgramAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ("name",)
    filter_horizontal = ("sites",)


@admin.register(models.Event)
class EventAdmin(admin.ModelAdmin):
    list_per_page = 10

    inlines = [EventRevisionsInline]


@admin.register(models.Report)
class ReportAdmin(admin.ModelAdmin):
    pass
