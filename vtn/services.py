import logging
from typing import List
from typing import Optional

from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import DataError
from django.utils import timezone
from django_x509 import models as x509_models

from . import models
from vtn import exceptions as vtn_exceptions

logger = logging.getLogger(__name__)


def verify_ven_request(ven_id: str, request, fingerprint=True) -> models.Site:
    # The VEN ID specified should always exist in our database
    try:
        site = models.Site.objects.get(ven_id=ven_id)
    except ObjectDoesNotExist:
        raise vtn_exceptions.InvalidVENRequest("No site with given VEN ID found")

    # Check the VEN ID specified matches the certificate fingerprint
    # XXX Use request.headers in Django 2.2
    if (
        fingerprint
        and "HTTP_X_CERTIFICATE_FINGERPRINT_SHA1" in request.META
        and not models.SiteCertificate.objects.filter(
            site__ven_id=ven_id,
            certificate_sha1_fingerprint=request.META[
                "HTTP_X_CERTIFICATE_FINGERPRINT_SHA1"
            ],
        ).exists()
    ):
        raise vtn_exceptions.InvalidVENRequest("Certificate fingerprint not recognised")

    return site


def get_new_ven_ID() -> str:
    all_ids = [int(s.ven_id) for s in models.Site.objects.all()]
    if all_ids == []:
        return "0"
    else:
        return str(max(all_ids) + 1)


def create_event(data: dict, selected_sites: List[int]) -> None:
    event = models.Event(**data)
    event.save()

    for site_id in selected_sites:
        models.SiteEvent.objects.create(
            event=event,
            site_id=site_id,
            status=models.SiteEvent.EVENT_FAR,
            last_status_time=timezone.now(),
            opt_in=models.SiteEvent.OPT_NEITHER,
        )

    return event.id


def remove_site_from_event(event_id: int, remove_site_pk: int):
    """Removes a site from an event - in effect opts it out
    :param event_id: the event_id - not the event.id.
    :param remove_site_id: The site.id - not the site.site_id
    """
    event = models.Event.objects.get(pk=event_id)

    models.SiteEvent.objects.filter(event=event, site_id=remove_site_pk).update(
        status=models.SiteEvent.EVENT_CANCELLED,
        ven_status=models.SiteEvent.VEN_STATUS_NOT_TOLD,
    )

    # In order to have this get picked up by the VEN we need to increment the modification_number
    _create_superseded_event(event)
    event.modification_reason = f"Site #{remove_site_pk} removed"
    event.save()


def resubscribe_site_to_event(event_id: int, restore_site_id: int):
    """Restores a site from an event that it has previously opted out of using the
    function above.
        :param event_id: the event PK
        :param restore_site_id: The site.id - not the site.site_id
    """

    event = models.Event.objects.get(pk=event_id)

    # Check that this site has actually opted out of this event (& both exist)
    if (
        models.SiteEvent.objects.filter(
            event=event,
            site_id=restore_site_id,
            status=models.SiteEvent.EVENT_CANCELLED,
        ).count()
        == 0
    ):
        # It didn't.
        raise vtn_exceptions.NotOptedOut

    models.SiteEvent.objects.filter(event_id=event_id, site_id=restore_site_id).update(
        status=models.SiteEvent.EVENT_FAR,
        ven_status=models.SiteEvent.VEN_STATUS_NOT_TOLD,
    )

    # In order to have this get picked up by the VEN we need to increment the modification_number
    _create_superseded_event(event)
    event.modification_reason = f"Site #{restore_site_id} resubscribed"
    event.save()


def update_event(
    event_id: int,
    data: dict,
    selected_sites: Optional[List[int]] = None,
    reason: str = "",
) -> None:
    event = models.Event.objects.get(pk=event_id)

    # Deleted events can't be modified
    if event.deleted_at:
        raise DataError

    # First up - has any of the event's data changed? If so create a revision
    updatable_fields = [
        "start",
        "end",
        "program_id",
        "scheduled_notification_time",
        "status",
    ]

    updated_data = {}
    create_new_revision = False

    # If we were sent a list of sites, sync them:
    if selected_sites is not None:
        if _update_sites(event, selected_sites) is True:
            create_new_revision = True
            reason = reason or "Sites changed"

    for k in updatable_fields:
        if k in data and getattr(event, k) != data.get(k):
            updated_data[k] = data[k]

            # Bit of a wrinkle here: status changes only trigger a new revision
            # if being set to 'cancelled' (OADR rule 5)
            if k != "status" or data.get("status") == models.Event.EVENT_CANCELLED:
                create_new_revision = True

    if create_new_revision:
        _create_superseded_event(event)
        updated_data["modification_reason"] = reason

    if updated_data:
        for k, v in updated_data.items():
            setattr(event, k, v)
        event.save()

    # Did we delete the event? No need for a revision for this
    if "deleted_at" in data.keys() and data["deleted_at"]:
        event.deleted = timezone.now()
        event.save()


def _update_sites(event: models.Event, selected_sites: List[int]) -> bool:
    """Only to be called from the above function. Returns True if any
    modifications made.
    """

    modified = False

    active_sites = list(
        models.SiteEvent.objects.filter(event=event)
        .exclude(status=models.SiteEvent.EVENT_CANCELLED)
        .values_list("site_id", flat=True)
    )
    inactive_sites = list(
        models.SiteEvent.objects.filter(
            event=event, status=models.SiteEvent.EVENT_CANCELLED
        ).values_list("site_id", flat=True)
    )

    sites_to_add = set(selected_sites) - set(active_sites) - set(inactive_sites)
    sites_to_update = set(inactive_sites).intersection(set(selected_sites))
    sites_to_remove = set(active_sites) - set(selected_sites)

    # Opt these back in if they were previously opted out:
    if sites_to_update:
        modified = True
        models.SiteEvent.objects.filter(
            event=event, site_id__in=sites_to_update
        ).update(
            status=models.SiteEvent.EVENT_SCHEDULED,
            last_status_time=timezone.now(),
            opt_in=models.SiteEvent.OPT_NEITHER,
            ven_status=models.SiteEvent.VEN_STATUS_NOT_TOLD,
        )

    # Create them if they weren't:
    for site_id in sites_to_add:
        models.SiteEvent.objects.create(
            event=event,
            site_id=site_id,
            status=models.SiteEvent.EVENT_SCHEDULED,
            last_status_time=timezone.now(),
            opt_in=models.SiteEvent.OPT_NEITHER,
            ven_status=models.SiteEvent.VEN_STATUS_NOT_TOLD,
        )
        modified = True

    # Removing sites actually means opting them out
    # (we don't want sites to be simply deleted from an event once added)
    if sites_to_remove:
        modified = True
        models.SiteEvent.objects.filter(event=event, site__in=sites_to_remove).update(
            event=event,
            status=models.SiteEvent.EVENT_CANCELLED,
            ven_status=models.SiteEvent.VEN_STATUS_NOT_TOLD,
        )

    return modified


def _create_superseded_event(event) -> None:
    models.SupersededEvent.objects.create(
        event=event,
        start=event.start,
        end=event.end,
        scheduled_notification_time=event.scheduled_notification_time,
        program=event.program,
        status=event.status,
        modification_reason=event.modification_reason,
    )


def cancel_event(event_id: int) -> None:
    """Use the previous function to create a new revision with a cancelled
    status & no sites"""
    update_event(event_id, {"status": models.Event.EVENT_CANCELLED}, [])

    # should the Site Event be set to deleted? Doesn't seem to be used anywhere


def update_notification_sent_time(site_events) -> None:
    """
    site_events is a QuerySet of SiteEvent
    """
    site_events.update(
        notification_sent_time=timezone.now(),
        ven_status=models.SiteEvent.VEN_STATUS_TOLD,
    )


def update_last_status_time(ven_id: str) -> None:
    models.Site.objects.filter(ven_id=ven_id).update(last_status_time=timezone.now())


def generate_cert(
    ca: x509_models.Ca, site: models.Site, *, replace: bool = False
) -> models.SiteCertificate:
    cert = models.Cert.objects.create(ca=ca, name=f"ven_{site.ven_id}")
    # Nginx passes on a fingerprint which is lowercased and has no colons, so
    # we have to transform the OpenSSL fingerprint here.
    # https://www.pyopenssl.org/en/stable/api/crypto.html#OpenSSL.crypto.X509.digest
    sha1_fingerprint = cert.x509.digest("sha1").decode("utf-8").lower().replace(":", "")
    sha256_fingerprint = (
        cert.x509.digest("sha256").decode("utf-8").lower().replace(":", "")
    )

    if replace:
        site_certs = models.SiteCertificate.objects.filter(site=site)
        x509_models.Cert.objects.filter(sitecertificate__in=site_certs).delete()
        # Because SiteCertificate.cert cascades deletion, we don't have to delete
        # them too, the DB does it for us.

    return models.SiteCertificate.objects.create(
        site=site,
        cert=cert,
        certificate_sha1_fingerprint=sha1_fingerprint,
        certificate_sha256_fingerprint=sha256_fingerprint,
    )
