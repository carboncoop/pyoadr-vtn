# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django_rq import get_scheduler


class VtnConfig(AppConfig):
    name = "vtn"

    def ready(self):
        from .tasks import update_event_statuses, update_online_offline

        scheduler = get_scheduler("default")

        if "vtn:update_event_statuses" not in scheduler:
            scheduler.cron(
                "30 * * * *",  # At half past, every hour
                update_event_statuses,
                id="vtn:update_event_statuses",
            )

        if "vtn:update_online_offline" not in scheduler:
            scheduler.cron(
                "30 * * * *",  # At half past, every hour
                update_online_offline,
                id="vtn:update_online_offline",
            )
