Glossary
========
.. glossary::


  DER
    Distributed Energy Resources: Controllable load and micro-generation.

  Flexibility
    The commodification of the ability to adjust demand or generation dynamically in response to external signals or information.

  VTN
    Virtual Top Node: as defined in OpenADR, any software component acting as a control server providing certain defined services to VEN clients.

  VEN
    Virtual End Node: as defined in OpenADR, a software client communicating with a VTN.

  OpenADR
    Open Automated Demand Response is a research and standards development effort for energy management led by North American research labs and companies. The typical use is to send information and signals to cause electrical power-using devices to be turned off during periods of high demand.

  DRMS
    Demand Response Management System: A generic term often used in North America to describe the control panel of a demand side response system. This application is a DRMS.

