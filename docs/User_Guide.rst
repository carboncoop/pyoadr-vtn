.. _VtnServerGuide:

User Guide
==========

.. toctree::
   :maxdepth: 1

   Secure_Gateway
   Certificates
   Management_API


Logging In
----------
This guide assumes that you have a valid user account to access and log in to the VTN application website.

If you have followed the instructions in Development_Environment, your VTN instance should appear at http://localhost:3000.

A superuser and password will have been created:

.. code:: bash

    user: root
    password: password

Overview Screen
---------------

Once logged in for the first time, this is the ‘Overview’ screen.

In order to begin scheduling DR events, one must have at least one customer,
with at least one associated site/VEN, and at least one sort of demand response (DR) program.

A VTN will not be able to tell a VEN about DR Events if the VTN doesn’t know about the VEN.
A VTN knows about a VEN after a Site for the VEN has been created in the VTN application,
and the VEN has contacted the VTN.

This can be done manually or by using the Management API.

The rest of this document describes how to set up Customers, Sites, DR Programs,
and DR Events, manually, as well as how to export event data.

Create a Customer
-----------------

Click on the "Add Customer" button to add a customer.

Customers can also be created using the Management API.

Create a Site
-------------

Click into the customer you want to add a site for and click "Create New Site".
A site must have a unique `VEN ID`.
Add the site to a program.

Sites can also be created using the Management API

Create a DR Program
-------------------

DR Programs must be added via the Admin interface at VTN > DR Programs.

DR Programs can be added with or without associated sites. In other words, a DR Program can be created with no sites,
and sites can be added later, either by Creating/Editing a Site and selecting
the DR Program, or by Creating/Editing the DR Program and adding the Site.

The type of event can be changed by editing the "signal payload" field when creating an event.


Create a DR Event
-----------------

Once a Customer (with at least one site) and a DR Program have been added, a DR Event
can be created. This is done by navigating to the **Overview** screen and clicking ‘Add DR Event’.

On the **Add DR Event** screen, the first step is to select a DR Program from the drop-down menu.
Once a DR Program is selected, the ‘Sites’ multi-select box will auto-populate with
the Sites that are associated with that DR Program.

*Note that the Notification Time is the absolute soonest time that a VEN will be*
*notified of a DR Event.  VENs will not ‘know’ about DR Events that apply to them*
*until they have ‘polled’ the VTN after the Notification Time.*

**All times are in UTC**

Active DR events are displayed on the **Overview** screen.  DR Events are considered active
if they have not been cancelled and if they have not been completed.

Reporting Telemetry
-------------------
Exporting event telemetry to a .csv is available on the **Export** tab.
This is an area that requires more development.



