.. PyOADR-VTN documentation master file, created by
   sphinx-quickstart on Mon Feb 10 14:26:21 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyOADR-VTN
==========



.. include:: ../README.rst

****


.. toctree::
   :maxdepth: 1

   Developing
   User_Guide
   Glossary
   contributing

****

OpenADR Client Implementation
-----------------------------

This project has been developed alongside a PyOADR-VEN - a python implementation of the :term:`OpenADR` :term:`VEN` client.

This client is available as a `PyPi Package`_, complete with it's own `documentation`_ site.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



.. _PyPi Package: https://pypi.org/project/pyoadr-ven/
.. _documentation: https://carboncoop.gitlab.io/pyoadr-ven/
