Management API
==============

A management API is available at:

``/api/``

Swagger Documentation for this API is available at:

``/api/docs``

To see this documentation, start the django development server and navigate to:

    $ http://localhost/api/docs


API Authentication
------------------
The API uses an API key for authentication.
You can create a new API key in Django Admin.

API Schema
----------
The schema for the api is available at:

``/openapi``
