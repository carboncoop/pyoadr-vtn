Secure Gateway
==============
The VTN application is behind a secure gateway which is a custom configuration of an nginx reverse proxy. This enables a
few different features:

- Efficienct and correct client certificate checking: the nginx proxy efficiently off-loads the client certificate checking and
  passes the SHA1 of the client certificates to the VTN application for authentication/authorisation.
- Load balancing: the nginx proxy can load balance across VTN applications.
- Security: the nginx proxy acts as a secure gateway with higher security guarantees and a smaller attack surface than a
  normal web application. All VTN requests must pass through the gateway which makes it easy to detect attacks etc. It also
  implements a layered approach to security which provides high security guarantees at each level as well as architecturally.

Access in browser
------------------
To access the VTN UI in a browser you will need to create a console user with an associated signed client certificate.
An administrative user can do this from the VTN server and issue a P12 bundle

FIX: Currently this is not fully implemented so it is sufficient to use a dummy VEN certificate for now and create a P12
bundle using the instructions below and then import this. You will still additionally need to authorise/authenticate with the
VTN application using an existing account.

Creating a P12 bundle
~~~~~~~~~~~~~~~~~~~~~
To import the PEM files into a browser they need to be bundled in the P12/PFX format. On the command line you can generate
a bundle as follows:

.. code:: bash

    openssl pkcs12 -export -out client.p12 -inkey client.key -in client.crt

- client.key: this is the private client key issued by the VTN.
- client.crt: this is the signed client certificate issed by the VTN.

Import into browser
~~~~~~~~~~~~~~~~~~~
Firefox e.g. https://www.sslsupportdesk.com/how-to-import-a-certificate-into-firefox/
Chrome e.g. https://support.securly.com/hc/en-us/articles/206081828-How-to-manually-install-the-Securly-SSL-certificate-in-Chrome

Nginx Configuration
-------------------
Certain aspects of the nginx configuration used here are specialised and worth noting:

- The nginx setup is configured to run in a docker as part of a set of containers. The other containers are: a 'letsencrypt'
  service which provisions/checks/renews letsencrypt certificates which are used as the server certificates; a 'crl' container
  which periodically downloads the certificate revocation list from the internal PKI.
- As noted elsewhere, we do not sign client certificates with the server certificate as is common in other implementations.
  Our server certifiicates are provided by letencrypt (part of the internet public key infrastructure) and our client certificates
  are signed by a separate root and intermediate certificates under our control. This is more reflective of modern security practices
  and provides additional security guarantees (self signing both server and client certificates might be seen as security through obscurity).
