Development Environment
=======================

Ready to contribute? Here's how to set up `pyoadr_vtn` for local development.

#. Fork the `pyoadr_vtn repository`_ repo on Gitlab.
#. Clone your fork locally

#. Install your local copy into a virtualenv.
Assuming you have virtualenvwrapper installed, this is how you set up your fork for local development:

.. code:: bash

    mkvirtualenv pyoadr_vtn
    cd pyoadr_vtn/
    pip-sync requirements/local.txt

#. Install the node packages required by running:

.. code:: bash

    npm install package-lock.json

#. This project uses pre-commit to enforce black and flake8 on the codebase.
   Install pre-commit with:

.. code:: bash

    pip install pre-commit
    pre-commit install

5. Create a branch for local development:

.. code:: bash

    git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally.

6. Run the Django development server with:

.. code:: bash

    docker-compose -f local.yml up

    The development/testing environment will create local PostgreSQL (DB), Redis (cache/celery tasks), and nginx (reverse proxy)

To run a local development version:

.. code:: bash

    npm run dev

To run local tests:

.. code:: bash

    docker-compose -f local.yml -f test.yml up

6. To check you're passing the tests run:

.. code:: bash

    docker-compose -f local.yml -f test.yml up

or

.. code:: bash

    npm run dev
    pytest

7. Commit your changes and push your branch to GitLab:

.. code:: bash

    git add .
    git commit -m "Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature

8. Submit a pull request through the GitLab website.

Merge Request Guidelines
------------------------

Before you submit a merge request, check that it meets these guidelines:

1. The merge request should include tests.
2. If the merge request adds functionality, the docs should be updated.



.. _pyoadr_vtn repository: https://gitlab.com/carboncoop/pyoadr_vtn
