Python OpenADR Virtual Top Node Implementation
==============================================

* Free software: Apache Software License 2.0
* Module Documentation: https://carboncoop.gitlab.io/pyoadr-vtn/
* Project Documentation: https://carboncoop.gitlab.io/opendsr/docs/


OpenADR (Automated Demand Response) is a standard for alerting and responding
to the need to adjust electric power consumption in response to fluctuations
in grid demand.

OpenADR communications are conducted between Virtual Top Nodes (VTNs) and Virtual End Nodes (VENs).


This application is an open source implementation of an OpenADR VTN server, written in Python and Django.

It implements EiEvent and EiReport services in conformance with a subset of the OpenADR 2.0b specification.



Credits
-------
* Carbon Co-op
* Battelle Memorial Institute
