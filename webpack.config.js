module.exports = {
    entry: './vtn/static/js/project.js',
    output: {
        filename: 'project.min.js',
        path: __dirname + '/vtn/static/js/',
    },
    module: {
        rules: [
            {
                test: /bootstrap\.native/,
                use: {
                    loader: 'bootstrap.native-loader',
                    options: {
                        only: ['modal', 'dropdown'],
                    },
                },
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/plugin-proposal-class-properties'],
                    },
                },
            },
        ],
    },
}
