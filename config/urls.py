from django.conf import settings
from django.contrib import admin
from django.urls import include
from django.urls import path
from rest_framework.documentation import include_docs_urls

from vtn.views import HealthCheck

urlpatterns = [
    # OpenADR functionality
    path("", include("vtn.openadr.urls", namespace="openadr")),
    # Administrative user interfaces
    path("", include("vtn.ui.urls", namespace="vtn")),
    path("admin/", admin.site.urls),
    path("admin/django-rq/", include("django_rq.urls")),
    # Management api to create customers, sites etc
    path("api/", include("vtn.api.urls", namespace="api")),
    path("api/docs/", include_docs_urls(title="VTN Management API")),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    # django-x509 CRL URLs
    # These are annoyingly required for the admin backend, but not anything else.
    # We have our own API endpoints for CRLs now at /api/ca/crl, see vtn#26
    path("", include("django_x509.urls", namespace="x509")),
    path("healthcheck", HealthCheck.as_view(), name="healthcheck"),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]
