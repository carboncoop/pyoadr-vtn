import logging

import requests
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.logging import LoggingIntegration
from sentry_sdk.integrations.rq import RqIntegration
from sentry_sdk.integrations.redis import RedisIntegration
from .base import *  # noqa
from .base import env  # noqa

# GENERAL
# ------------------------------------------------------------------------------
#
ENV = env("ENV")  # can also be 'staging'

# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env("DJANGO_SECRET_KEY")

if ENV == "production":
    SITE_URL = env("SITE_URL", default="openadr.carbon.coop")
else:
    SITE_URL = env("SITE_URL", default="openadr-staging.carbon.coop")

# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list(
    "DJANGO_ALLOWED_HOSTS",
    default=[
        SITE_URL,
        "openadr.carboncoop",
        "openadr.openadr",
        "openadr-staging.carboncoop",
        "openadr-staging.openadr-staging",
        "nocert.openadr-staging.carboncoop",
        "nocert.openadr-staging.carbon.coop",
        "nocert.openadr.carbon.coop",
        "nocert.openadr.carboncoop",
    ],
)

LOCAL_IP = None

try:
    LOCAL_IP = requests.get(
        "http://169.254.169.254/latest/meta-data/local-ipv4", timeout=0.01
    ).text
except requests.exceptions.RequestException:
    pass
if LOCAL_IP and not DEBUG:  # noqa F405
    ALLOWED_HOSTS.append(LOCAL_IP)

LOCAL_HOSTNAME = None
try:
    LOCAL_HOSTNAME = requests.get(
        "http://169.254.169.254/latest/meta-data/local-hostname", timeout=0.01
    ).text
except requests.exceptions.RequestException:
    pass
if LOCAL_HOSTNAME and not DEBUG:  # noqa F405
    ALLOWED_HOSTS.append(LOCAL_HOSTNAME)


# DATABASES
# ------------------------------------------------------------------------------
DATABASES["default"] = env.db("DATABASE_URL")  # noqa F405
DATABASES["default"]["ATOMIC_REQUESTS"] = True  # noqa F405
DATABASES["default"]["CONN_MAX_AGE"] = env.int("CONN_MAX_AGE", default=60)  # noqa F405
REDIS_HOST = "cc-redis-cluster-001.cc-redis-cluster.q2rtcj.euw2.cache.amazonaws.com"

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "rediss://" + REDIS_HOST + ":6379",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            # Mimicing memcache behavior.
            # http://niwinz.github.io/django-redis/latest/#_memcached_exceptions_behavior
            "IGNORE_EXCEPTIONS": True,
            "PASSWORD": env.str("REDIS_PASSWORD"),
            "REDIS_CLIENT_KWARGS": {"ssl": True},
        },
    }
}
if ENV == "production":
    CACHES["default"]["OPTIONS"]["DB"] = 8
else:
    CACHES["default"]["OPTIONS"]["DB"] = 9


# SECURITY
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-proxy-ssl-header
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-ssl-redirect
SECURE_SSL_REDIRECT = env.bool("DJANGO_SECURE_SSL_REDIRECT", default=True)
# https://docs.djangoproject.com/en/2.2/ref/settings/#secure-redirect-exempt
SECURE_REDIRECT_EXEMPT = [r"^api/.*$"]
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-secure
SESSION_COOKIE_SECURE = True
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-secure
CSRF_COOKIE_SECURE = True
# https://docs.djangoproject.com/en/dev/topics/security/#ssl-https
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-seconds
# TODO: set this to 60 seconds first and then to 518400 once you prove the former works
SECURE_HSTS_SECONDS = 60
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-include-subdomains
SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
    "DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS", default=True
)
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-preload
SECURE_HSTS_PRELOAD = env.bool("DJANGO_SECURE_HSTS_PRELOAD", default=True)
# https://docs.djangoproject.com/en/dev/ref/middleware/#x-content-type-options-nosniff
SECURE_CONTENT_TYPE_NOSNIFF = env.bool(
    "DJANGO_SECURE_CONTENT_TYPE_NOSNIFF", default=True
)

# STORAGES
# ------------------------------------------------------------------------------
# https://django-storages.readthedocs.io/en/latest/#installation
INSTALLED_APPS += ["storages"]  # noqa F405
# AWS_ACCESS_KEY_ID = env("VTN_DJANGO_AWS_ACCESS_KEY_ID")
# AWS_SECRET_ACCESS_KEY = env("VTN_DJANGO_AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = "openadr-carboncoop"
AWS_DEFAULT_ACL = "public-read"
AWS_QUERYSTRING_AUTH = False
# DO NOT change these unless you know what you're doing.
_AWS_EXPIRY = 60 * 60 * 24 * 7
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_S3_OBJECT_PARAMETERS = {
    "CacheControl": f"max-age={_AWS_EXPIRY}, s-maxage={_AWS_EXPIRY}, must-revalidate"
}

# STATIC
# ------------------------
STATICFILES_STORAGE = "config.settings.production.StaticRootS3Boto3Storage"
STATIC_URL = f"https://openadr-carboncoop.s3.amazonaws.com/static-{ENV}/"

# MEDIA
# ------------------------------------------------------------------------------
# region http://stackoverflow.com/questions/10390244/
# Full-fledge class: https://stackoverflow.com/a/18046120/104731
from storages.backends.s3boto3 import S3Boto3Storage  # noqa E402


class StaticRootS3Boto3Storage(S3Boto3Storage):
    location = f"static-{ENV}"


class MediaRootS3Boto3Storage(S3Boto3Storage):
    location = f"media-{ENV}"
    file_overwrite = False


# endregion
DEFAULT_FILE_STORAGE = "config.settings.production.MediaRootS3Boto3Storage"
MEDIA_URL = f"https://openadr-carboncoop.s3.amazonaws.com/media-{ENV}/"

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = env(
    "DJANGO_DEFAULT_FROM_EMAIL", default="Carbon Co-op <info@carbon.coop>"
)
# https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = env("DJANGO_SERVER_EMAIL", default=DEFAULT_FROM_EMAIL)
# https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
EMAIL_SUBJECT_PREFIX = env(
    "DJANGO_EMAIL_SUBJECT_PREFIX", default="[Carbon Co-op VTN DRMS]"
)

# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL regex.
ADMIN_URL = env("DJANGO_ADMIN_URL", default="djadmin/")

# Anymail (Mailgun)
# ------------------------------------------------------------------------------
# https://anymail.readthedocs.io/en/stable/installation/#installing-anymail
INSTALLED_APPS += ["anymail"]  # noqa F405
EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"
# https://anymail.readthedocs.io/en/stable/installation/#anymail-settings-reference
ANYMAIL = {
    "MAILGUN_API_KEY": env("MAILGUN_API_KEY"),
    "MAILGUN_API_URL": env("MAILGUN_API_URL", default="https://api.eu.mailgun.net/v3"),
    "MAILGUN_SENDER_DOMAIN": env("MAILGUN_DOMAIN", default="mg.carbon.coop"),
}

# Gunicorn
# ------------------------------------------------------------------------------
INSTALLED_APPS += ["gunicorn"]  # noqa F405

# django-compressor
# ------------------------------------------------------------------------------
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_ENABLED
# COMPRESS_ENABLED = env.bool("COMPRESS_ENABLED", default=True)
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_STORAGE
# COMPRESS_STORAGE = "config.settings.production.StaticRootS3Boto3Storage"
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_URL
# COMPRESS_URL = STATIC_URL
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_OUTPUT_DIR
# COMPRESS_OUTPUT_DIR = "cache"

# Collectfast
# ------------------------------------------------------------------------------
# https://github.com/antonagestam/collectfast#installation
# INSTALLED_APPS = ["collectfast"] + INSTALLED_APPS  # noqa F405
# AWS_PRELOAD_METADATA = True

# Logging
# ------------------------------------------------------------------------------
LOGGING = {  # noqa
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        }
    },
    "loggers": {
        "django": {
            "handlers": ["console"],
            "level": env("DJANGO_LOG_LEVEL", default="WARN"),
            "propagate": False,
        },
        "django.db.backends": {
            "level": "ERROR",
            "handlers": ["console"],
            "propagate": False,
        },
        "sentry_sdk": {"level": "ERROR", "handlers": ["console"], "propagate": False},
        "django.security.DisallowedHost": {
            # This disables sending DisallowedHost errors to Sentry
            "level": "ERROR",
            "handlers": ["console"],
            "propagate": False,
        },
    },
}
SENTRY_DSN = env("SENTRY_DSN")
SENTRY_LOG_LEVEL = env.int("DJANGO_SENTRY_LOG_LEVEL", logging.INFO)
RQ_SENTRY_DSN = env("SENTRY_DSN")

sentry_logging = LoggingIntegration(
    level=SENTRY_LOG_LEVEL,  # Capture info and above as breadcrumbs
    event_level=None,  # Send no events from log messages
)

sentry_sdk.init(
    dsn=SENTRY_DSN,
    integrations=[sentry_logging, DjangoIntegration(), RqIntegration(), RedisIntegration()],
    environment=ENV,
)

# Your stuff...
# ------------------------------------------------------------------------------

# RQ
# ----------------------------------------------------------------------
RQ_QUEUES["default"]["HOST"] = REDIS_HOST  # noqa  # noqa
RQ_QUEUES["default"]["PORT"] = 6379  # noqa
RQ_QUEUES["default"]["SSL"] = True  # noqa
if ENV == "production":
    RQ_QUEUES["default"]["DB"] = 8  # noqa
else:
    RQ_QUEUES["default"]["DB"] = 9  # noqa

# CORS
# ----------------------------------------------------------------------
INSTALLED_APPS += ["corsheaders"]
MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
] + MIDDLEWARE  # noqa
CORS_ORIGIN_WHITELIST = [
    "https://openadr-staging.carbon.coop",
    "https://openadr.carbon.coop",
    "https://openadr-carboncoop.s3.amazonaws.com",
    "http://localhost:8080",
    "http://127.0.0.1:8080",
]
