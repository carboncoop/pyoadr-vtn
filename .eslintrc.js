module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    extends: ['eslint:recommended', 'plugin:compat/recommended'],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parser: 'babel-eslint',
    rules: {
        indent: ['error', 4, { SwitchCase: 1 }],
        'linebreak-style': ['error', 'unix'],
        quotes: ['error', 'single', 'avoid-escape'],
        semi: ['error', 'never'],
        'no-console': 'off',
    },
    plugins: ['compat'],
    settings: {
        // https://github.com/amilajack/eslint-plugin-compat#adding-polyfills
        polyfills: ['Promise', 'fetch'],
    },
    overrides: [
        {
            files: ['gulpfile.js', 'webpack.config.js', '.eslintrc'],
            env: {
                browser: false,
                node: true,
            },
        },
    ],
}
