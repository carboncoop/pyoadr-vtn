////////////////////////////////
// Setup
////////////////////////////////

// Mode
const mode = process.env.NODE_ENV || 'dev'

// Gulp and package
const { src, dest, parallel, series, watch } = require('gulp')

// Plugins
const autoprefixer = require('autoprefixer')
const csso = require('postcss-csso')
const pixrem = require('pixrem')
const pjson = require('./package.json')
const plumber = require('gulp-plumber')
const postcss = require('gulp-postcss')
const rename = require('gulp-rename')
const sass = require('gulp-sass')
const spawn = require('child_process').spawn
const webpack = require('webpack')
const webpackConfig = require('./webpack.config.js')
const webpackStream = require('webpack-stream')

let browserSync
let reload
let imagemin

// We don't install everything when building docker containers
try {
    imagemin = require('gulp-imagemin')
    browserSync = require('browser-sync')
} catch (e) {
    if (e instanceof Error && e.code === 'MODULE_NOT_FOUND') {
        console.log("Didn't find one of: imagemin, browser-sync")
    } else {
        throw e
    }
}

// Initialise BrowserSync if we have it
if (browserSync) {
    browserSync = browserSync.create()
    reload = browserSync.reload
}

// Set production or dev for webpack
webpackConfig.mode = mode ? 'production' : 'development'

// Relative paths function
function pathsConfig(appName) {
    this.app = './' + (appName || pjson.name)
    var vendorsRoot = 'node_modules/'

    return {
        bootstrapSass: vendorsRoot + '/bootstrap/scss',
        starabilitySass: vendorsRoot + '/starability/starability-scss',

        vendorsJs: [
            vendorsRoot + 'jquery/dist/jquery.slim.js',
            vendorsRoot + 'popper.js/dist/umd/popper.js',
            vendorsRoot + 'bootstrap/dist/js/bootstrap.js',
        ],

        app: this.app,
        templates: this.app + '/templates',
        css: this.app + '/static/css',
        sass: this.app + '/static/sass',
        fonts: this.app + '/static/fonts',
        images: this.app + '/static/images',
        js: this.app + '/static/js',
    }
}

const paths = pathsConfig()

////////////////////////////////
// Tasks
////////////////////////////////

// Styles autoprefixing and minification
function styles() {
    var processCss = [
        autoprefixer(), // adds vendor prefixes
        pixrem(), // add fallbacks for rem units
    ]

    var minifyCss = [
        csso({ restructure: false }), // minify result
    ]

    return src(`${paths.sass}/project.scss`)
        .pipe(
            sass({
                includePaths: [
                    paths.bootstrapSass,
                    paths.starabilitySass,
                    paths.sass,
                ],
            }).on('error', sass.logError)
        )
        .pipe(plumber()) // Checks for errors
        .pipe(postcss(processCss))
        .pipe(dest(paths.css))
        .pipe(rename({ suffix: '.min' }))
        .pipe(postcss(minifyCss))
        .pipe(dest(paths.css))
}

// Javascript minification
function scripts() {
    return src(`${paths.js}/project.js`)
        .pipe(
            webpackStream(webpackConfig),
            webpack
        )
        .pipe(dest(paths.js))
}

// Image compression
function imgCompression() {
    return src(`${paths.images}/*`)
        .pipe(imagemin()) // Compresses PNG, JPEG, GIF and SVG images
        .pipe(dest(paths.images))
}

// Run django server
function runServer(cb) {
    const cmd = spawn('python', ['-Wd', 'manage.py', 'runserver', 'localhost:8001'], {
        stdio: 'inherit',
    })
    cmd.on('close', function(code) {
        console.log('runServer exited with code ' + code)
        cb(code)
    })
}

// Browser sync server for live reload
function initBrowserSync() {
    browserSync.init(
        [
            `${paths.css}/*.css`,
            `${paths.js}/*.min.js`,
            `${paths.templates}/*.html`,
        ],
        {
            proxy: 'localhost:8001',
        }
    )
}

// Watch
function watchPaths() {
    watch(`${paths.sass}/*.scss`, styles)
    watch(
        [`${paths.js}/*.js`, `${paths.js}/**/*.js`, `!${paths.js}/*.min.js`],
        scripts
    ).on('change', reload)
    watch(`${paths.templates}/**/*.html`).on('change', reload)
}

// Generate assets
const generateAssets = parallel(styles, scripts)

const dev = parallel(runServer, initBrowserSync, watchPaths)

exports.default = series(generateAssets, imgCompression, dev)
exports['generate-assets'] = generateAssets
exports['dev'] = dev
