from vtn.models import Customer
from vtn.models import Program
from vtn.models import Site

customerNames = [
    "Clark Kent",
    "Peter Parker",
    "Anh Nguyen",
    "Bob Barcklay",
    "Nate Hill",
    "James Sheridan",
]
utility_id = ["001", "002", "003", "004", "005", "006"]

for i in range(0, 6):
    customer = Customer(name=customerNames[i], utility_id=utility_id[i])
    customer.save()

programs = [
    "capacity bidding program",
    "peak day pricing program",
    "direct load control",
    "emergency demand reponse",
]

site_names = [
    "Montclair",
    "Oakland",
    "San Francisco",
    "Moraga",
    "Emeryville",
    "Berkeley",
]
site_ids = [("site00" + str(x)) for x in range(1, 7)]
locations = [("location00" + str(x)) for x in range(1, 7)]
ipAddresses = [("274.524.501." + str(x)) for x in range(1, 7)]
site_address1s = [("121" + str(x) + " Sunnyhills Rd.") for x in range(0, 6)]
city = ["Oakland"] * 6
state = ["CA"] * 6
post_code = ["94610"] * 6
contact_names = [("Contact" + str(x)) for x in range(1, 7)]
phone_numbers = [("510325405" + str(x)) for x in range(1, 7)]
online = [True, False, True, False, True, False]
program_names = [("program" + str(x)) for x in range(0, 6)]

# build Sites
for x in range(0, 6):
    customer = Customer.objects.get(pk=x + 1)
    s = Site(
        customer=customer,
        site_name=site_names[x],
        site_id=site_ids[x],
        site_location_code=locations[x],
        ip_address=ipAddresses[x],
        site_address1=site_address1s[x],
        city=city[x],
        state=state[x],
        post_code=post_code[x],
        contact_name=contact_names[x],
        phone_number=phone_numbers[x],
        online=online[x],
    )
    s.save()

    d = Program(name=program_names[x])
    d.save()
    d.sites.add(s)
    d.save()
