FROM python:3.7-slim

# We're using "Slim" (which is a cut down Debian Stretch) because Debian is a
# much more standard setup than alpine with the potential for a lot less faff
# later on if we need to install more obscure dependencies.
FROM python:3.7-slim

# Don't buffer output - we should always get error messages this way
ENV PYTHONUNBUFFERED 1

# Don't write bytecode to disk
ENV PYTHONDONTWRITEBYTECODE 1

# Set up our user
RUN addgroup --system django \
    && adduser --system --ingroup django django

# Requirements are installed here to ensure they will be cached.
COPY ./requirements /app/requirements
WORKDIR /app
RUN pip install --no-cache-dir -r ./requirements/production.txt

COPY . /app
COPY ./docker/vtn/entrypoint /app/entrypoint
COPY ./docker/vtn/start /app/start
RUN chmod +x /app/entrypoint /app/start

RUN chown -R django /app

USER django

EXPOSE 8080

ENTRYPOINT ["/app/entrypoint"]
CMD ["/app/start"]
